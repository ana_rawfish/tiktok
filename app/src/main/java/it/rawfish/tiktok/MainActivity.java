package it.rawfish.tiktok;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.google.gson.JsonObject;

import it.rawfish.tiktok.fragments.LoginFragment;
import it.rawfish.tiktok.fragments.MainSectionsFragment;
import it.rawfish.tiktok.fragments.add_employee.AddEmployeeFragment;
import it.rawfish.tiktok.fragments.generic.GenerationInProgressDialog;
import it.rawfish.tiktok.fragments.generic.GenerationInProgressDialogBuilder;
import it.rawfish.tiktok.fragments.invoice.InvoiceDialogFragment;
import it.rawfish.tiktok.fragments.invoice.InvoiceDialogFragmentBuilder;
import it.rawfish.tiktok.fragments.invoice.OnInvoiceSessionListener;
import it.rawfish.tiktok.models.Employee;
import it.rawfish.tiktok.models.Invoice;
import it.rawfish.tiktok.models.InvoiceInitializers;
import it.rawfish.tiktok.models.NewEmployee;
import it.rawfish.tiktok.rest.RestBaseCallBack;
import it.rawfish.tiktok.rest.RestClient;
import it.rawfish.tiktok.utils.Constants;
import it.rawfish.tiktok.utils.SharedPrefsUtils;

public class MainActivity extends AppCompatActivity implements
		OnInvoiceSessionListener,
		LoginFragment.OnLoginListener,
		GenerationInProgressDialog.OnGenerationListener {

	private InvoiceDialogFragment mInvoiceFragment;
	private AddEmployeeFragment mEmployeeFragment;
	private FloatingActionButton mFab;
	private boolean mIsShowMenu = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		mFab = (FloatingActionButton) findViewById(R.id.fab);
		mFab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				runInvoiceQuiz(false);
			}
		});

		replaceFragment(new LoginFragment());
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		MenuItem itemLogout = menu.findItem(R.id.logout);
		MenuItem itemAdd = menu.findItem(R.id.add_user);
		itemLogout.setVisible(mIsShowMenu);

		if (Employee.getInstance().getRole() != null) {
			itemAdd.setVisible(Employee.getInstance().getRole().equals(Constants.ROLE_ADMIN));
		} else {
			itemAdd.setVisible(false);
		}

		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.logout:
				logout();
				return true;
			case R.id.add_user:
				addEmployee();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	private void addEmployee() {
		mEmployeeFragment = new AddEmployeeFragment();
		mEmployeeFragment.show(getSupportFragmentManager(), AddEmployeeFragment.TAG);
	}

	private void runInvoiceQuiz(final boolean isEditMode) {
		RestClient.get().getInvoiceInitData().enqueue(new RestBaseCallBack<InvoiceInitializers>() {
			@Override
			public void onSuccess(InvoiceInitializers obj) {
				InvoiceInitializers.get().setData(obj);
				mInvoiceFragment = new InvoiceDialogFragmentBuilder(isEditMode).build();
				mInvoiceFragment.show(getSupportFragmentManager(), InvoiceDialogFragment.TAG);
			}

			@Override
			public void onError(String error) {
			}
		});
	}

	@SuppressWarnings("unchecked")
	private void logout() {
		NewEmployee.getInstance().clear();
		Employee.getInstance().clear();
		Invoice.get().clear();
		SharedPrefsUtils.get().clear();

		assert getSupportActionBar() != null;
		getSupportActionBar().setTitle(getString(R.string.app_name));

		RestClient.get().postRevokeToken().enqueue(new RestBaseCallBack<JsonObject>() {
			@Override
			public void onSuccess(JsonObject obj) {
				replaceFragment(new LoginFragment());
				mIsShowMenu = false;
				invalidateOptionsMenu();

				if (mFab != null) {
					mFab.setVisibility(View.GONE);
				}
			}

			@Override
			public void onError(String error) {

			}
		});
	}

	public void replaceFragment(Fragment fragment) {
		getSupportFragmentManager()
				.beginTransaction()
				.replace(R.id.main_sections_container, fragment)
				.commitAllowingStateLoss();
	}

	@Override
	public void onCompleteInvoiceSession() {
		if (mInvoiceFragment != null)
			mInvoiceFragment.dismiss();

		mInvoiceFragment = null;

		replaceFragment(new MainSectionsFragment());
	}

	@Override
	public void onLoginSuccessfull() {
		mFab.setVisibility(View.VISIBLE);
		mIsShowMenu = true;
		invalidateOptionsMenu();

		assert getSupportActionBar() != null;
		getSupportActionBar().setTitle(getString(R.string.logged_in_as) + Employee.getInstance().getName());

		replaceFragment(new MainSectionsFragment());
	}

	@Override
	public void onShowGenerationDialog(String role, boolean isEdit) {
		new GenerationInProgressDialogBuilder(isEdit, role).build().show(
				getSupportFragmentManager(), GenerationInProgressDialog.TAG);
	}

	@Override
	public void onInvoiceGenerated() {
		onCompleteInvoiceSession();
	}

	@Override
	public void onEmployeeGenerated() {
		if (mEmployeeFragment != null)
			mEmployeeFragment.dismiss();

		mEmployeeFragment = null;
	}
}
