package it.rawfish.tiktok.adapters;


import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import it.rawfish.tiktok.R;
import it.rawfish.tiktok.databinding.ItemPhotoBinding;

public class GridImagesAdapter extends RecyclerView.Adapter<GridImagesAdapter.ItemViewHolder> {
	private ArrayList<String> mPaths = new ArrayList<>();
	private boolean mIsLastPh;
	private boolean mIsRemote;
	private OnPhotosAdapterListener mListener;

	public GridImagesAdapter(ArrayList<String> paths,
	                         boolean isRemote,
	                         boolean isLastPh,
	                         OnPhotosAdapterListener listener) {
		mPaths = paths;
		mListener = listener;
		mIsRemote = isRemote;
		mIsLastPh = isLastPh;
	}

	@Override
	public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		ItemPhotoBinding binding = ItemPhotoBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
		return new ItemViewHolder(binding);
	}

	@Override
	public void onBindViewHolder(ItemViewHolder holder, int position) {
		ItemPhotoBinding binding = holder.mBinding;

		if (position == mPaths.size() && mIsLastPh) {
			binding.photo.setScaleType(ImageView.ScaleType.CENTER_CROP);
			binding.photo.setImageResource(R.drawable.camera_ph);
			binding.getRoot().setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					mListener.onChooseMorePhotos();
				}
			});
		} else {
			if (mIsRemote)
				Picasso.with(binding.getRoot().getContext()).load(mPaths.get(position)).placeholder(R.drawable.camera_ph).into(binding.photo);
			else
				binding.photo.setImageURI(Uri.parse(mPaths.get(position)));
		}
	}

	@Override
	public int getItemCount() {
		return mIsLastPh ? mPaths.size() + 1 : mPaths.size();
	}

	public void addItems(ArrayList<String> uris) {
		mPaths.addAll(uris);
		notifyDataSetChanged();
	}

	public ArrayList<String> getPaths() {
		return mPaths;
	}

	public class ItemViewHolder extends RecyclerView.ViewHolder {
		public ItemPhotoBinding mBinding;

		public ItemViewHolder(ItemPhotoBinding binding) {
			super(binding.getRoot());
			mBinding = binding;
		}
	}

	public interface OnPhotosAdapterListener {
		void onChooseMorePhotos();
	}
}
