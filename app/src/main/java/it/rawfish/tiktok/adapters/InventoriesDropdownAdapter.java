package it.rawfish.tiktok.adapters;


import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

import it.rawfish.tiktok.databinding.ItemInventoryDropdownBinding;
import it.rawfish.tiktok.models.Inventory;

public class InventoriesDropdownAdapter extends BaseAdapter {
	private List<Inventory> mInventories = new ArrayList<>();

	public InventoriesDropdownAdapter(String firstItem, List<Inventory> objects) {
		mInventories.add(new Inventory(firstItem));
		mInventories.addAll(objects);
	}

	@Override
	public int getCount() {
		return mInventories.size();
	}

	@Override
	public Inventory getItem(int i) {
		return mInventories.get(i);
	}

	@Override
	public long getItemId(int i) {
		return mInventories.get(i).getId();
	}

	@NonNull
	@Override
	public View getView(int i, View convertView, @NonNull ViewGroup viewGroup) {
		final InventoryViewHolder holder;

		ItemInventoryDropdownBinding binding = ItemInventoryDropdownBinding.inflate(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
		holder = new InventoryViewHolder(binding);
		convertView = holder.mBinding.getRoot();

		if (i >= 0)
			holder.mBinding.setInventory(mInventories.get(i));

		return convertView;
	}

	private class InventoryViewHolder {
		ItemInventoryDropdownBinding mBinding;

		InventoryViewHolder(ItemInventoryDropdownBinding mBinding) {
			this.mBinding = mBinding;
		}
	}
}
