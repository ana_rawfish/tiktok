package it.rawfish.tiktok.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import it.rawfish.tiktok.databinding.ItemInventoryBinding;
import it.rawfish.tiktok.models.Inventory;


public class InventoriesRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

	private final OnItemsListListener mListener;
	private List<Inventory> mValues;

	public InventoriesRecyclerAdapter(List<Inventory> items, OnItemsListListener listener) {
		mValues = items;
		mListener = listener;
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		ItemInventoryBinding binding = ItemInventoryBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
		return new InventoryViewHolder(binding);
	}

	@Override
	public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
		if (holder != null) {
			if (holder instanceof InventoryViewHolder) {
				ItemInventoryBinding binding = ((InventoryViewHolder) holder).mBinding;
				binding.setInventory(mValues.get(position));
				binding.executePendingBindings();

				binding.getRoot().setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						mListener.onShowDetals(mValues.get(holder.getAdapterPosition()));
					}
				});
			}
		}
	}

	@Override
	public int getItemCount() {
		return mValues.size();
	}

	public class InventoryViewHolder extends RecyclerView.ViewHolder {
		public ItemInventoryBinding mBinding;

		public InventoryViewHolder(ItemInventoryBinding binding) {
			super(binding.getRoot());
			mBinding = binding;
		}
	}

	public interface OnItemsListListener {
		void onShowDetals(Inventory inventory);
	}
}
