package it.rawfish.tiktok.adapters;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.List;

import it.rawfish.tiktok.databinding.ItemInvoiceBinding;
import it.rawfish.tiktok.models.Invoice;
import it.rawfish.tiktok.rest.RestBaseCallBack;
import it.rawfish.tiktok.rest.RestClient;
import it.rawfish.tiktok.utils.Constants;

public class InvoicesRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

	private OnItemsListListener mListener;
	private List<Invoice> mValues;

	public InvoicesRecyclerAdapter(OnItemsListListener mListener, List<Invoice> mValues) {
		this.mListener = mListener;
		this.mValues = mValues;
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		ItemInvoiceBinding binding = ItemInvoiceBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
		return new InvoiceViewHolder(binding);
	}

	@Override
	public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
		if (holder instanceof InvoiceViewHolder) {
			final ItemInvoiceBinding binding = ((InvoiceViewHolder) holder).mBinding;
			binding.setInvoice(mValues.get(position));

			if (binding.getInvoice().getStatus() != null) {
				if (binding.getInvoice().getStatus().equals(Constants.INVOICE_STATUS_PENDING)) {
					binding.deliveredBtn.setVisibility(View.VISIBLE);
					binding.doneBtn.setVisibility(View.GONE);
					binding.deliveredBtn.setIndeterminateProgressMode(true);
					binding.deliveredBtn.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View view) {
							setInvoiceAsDone(binding);
						}
					});
				} else {
					binding.deliveredBtn.setVisibility(View.GONE);
					binding.doneBtn.setVisibility(View.VISIBLE);
				}
			}

			binding.getRoot().setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					mListener.onShowInvoiceActionsDialog(mValues.get(holder.getAdapterPosition()));
				}
			});
		}
	}

	private void setInvoiceAsDone(final ItemInvoiceBinding binding) {
		binding.deliveredBtn.setProgress(1);
		HashMap<String, String> status = new HashMap<String, String>(1);
		status.put("status", Constants.INVOICE_STATUS_DONE);
		RestClient.get().updateInvoiceStatus(binding.getInvoice().getId(), status).enqueue(new RestBaseCallBack<JsonObject>() {
			@Override
			public void onSuccess(JsonObject obj) {
				binding.deliveredBtn.setProgress(100);
				binding.deliveredBtn.setOnClickListener(null);
			}

			@Override
			public void onError(String error) {
				binding.deliveredBtn.setProgress(-1);
			}
		});
	}

	@Override
	public int getItemCount() {
		return mValues.size();
	}

	public class InvoiceViewHolder extends RecyclerView.ViewHolder {
		public ItemInvoiceBinding mBinding;

		public InvoiceViewHolder(ItemInvoiceBinding binding) {
			super(binding.getRoot());
			mBinding = binding;
		}
	}

	public interface OnItemsListListener {
		void onShowInvoiceActionsDialog(Invoice invoice);
	}
}
