package it.rawfish.tiktok.adapters;


import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;

import java.util.ArrayList;
import java.util.List;

import it.rawfish.tiktok.databinding.SimpleDropdownItemBinding;
import it.rawfish.tiktok.models.Part;

public class PartsAutocompleteArrayAdapter extends ArrayAdapter<Part> {
	private List<Part> mParts;

	public PartsAutocompleteArrayAdapter(Context context, int resource, List<Part> objects) {
		super(context, resource, objects);
		mParts = objects;
	}

	@Override
	public int getCount() {
		return mParts.size();
	}

	@Override
	public Part getItem(int i) {
		return mParts.get(i);
	}

	@Override
	public long getItemId(int i) {
		return i;
	}

	@NonNull
	@Override
	public View getView(int i, View convertView, @NonNull ViewGroup viewGroup) {
		final PartViewHolder holder;

		if (convertView == null) {
			SimpleDropdownItemBinding binding = SimpleDropdownItemBinding.inflate(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
			holder = new PartViewHolder(binding);
			convertView = holder.mBinding.getRoot();
			convertView.setTag(holder);
		} else {
			holder = (PartViewHolder) convertView.getTag();
		}

		holder.mBinding.text1.setText(mParts.get(i).getName());

		return convertView;
	}

	private class PartViewHolder {
		SimpleDropdownItemBinding mBinding;

		PartViewHolder(SimpleDropdownItemBinding mBinding) {
			this.mBinding = mBinding;
		}
	}

	@NonNull
	@Override
	public Filter getFilter() {
		return mNameFilter;
	}

	private Filter mNameFilter = new Filter() {
		@Override
		public String convertResultToString(Object resultValue) {
			return ((Part)(resultValue)).getName();
		}

		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			if(constraint != null) {
				ArrayList<Part> suggestions = new ArrayList<>();
				for (Part part : mParts) {
					if(part.getName().toLowerCase().startsWith(constraint.toString().toLowerCase())){
						suggestions.add(part);
					}
				}
				FilterResults filterResults = new FilterResults();
				filterResults.values = suggestions;
				filterResults.count = suggestions.size();
				return filterResults;
			} else {
				return new FilterResults();
			}
		}
		@Override
		protected void publishResults(CharSequence constraint, FilterResults results) {
			if(results != null && results.count > 0) {
				notifyDataSetChanged();
			}
		}
	};

}
