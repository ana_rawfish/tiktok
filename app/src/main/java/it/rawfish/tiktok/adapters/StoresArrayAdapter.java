package it.rawfish.tiktok.adapters;


import android.content.Context;
import android.support.annotation.Nullable;
import android.widget.ArrayAdapter;

import java.util.List;

import it.rawfish.tiktok.models.Store;

public class StoresArrayAdapter extends ArrayAdapter {
	List<Store> mValues;

	public StoresArrayAdapter(Context context, int resource, List<Store> objects) {
		super(context, resource, objects);

		mValues = objects;
	}

	@Nullable
	@Override
	public String getItem(int position) {
		return mValues.get(position).getName();
	}

	@Override
	public int getCount() {
		return mValues.size();
	}

	@Override
	public long getItemId(int position) {
		return mValues.get(position).getId();
	}
}
