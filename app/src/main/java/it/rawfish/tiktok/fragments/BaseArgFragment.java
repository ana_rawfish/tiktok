package it.rawfish.tiktok.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.hannesdorfmann.fragmentargs.FragmentArgs;
import com.hannesdorfmann.fragmentargs.annotation.Arg;

public class BaseArgFragment extends Fragment {

	@Arg
	public boolean mIsEditMode;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		FragmentArgs.inject(this);
	}

	public void showToast(String error) {
		if (getActivity() != null) {
			Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
		}
	}

	public boolean isEditMode() {
		return mIsEditMode;
	}

	public void setIsEditMode(boolean mIsEditMode) {
		this.mIsEditMode = mIsEditMode;
	}
}
