package it.rawfish.tiktok.fragments;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.ViewGroup;

import com.hannesdorfmann.fragmentargs.FragmentArgs;

import it.rawfish.tiktok.R;

public class BaseDialogFragment extends DialogFragment {

	@Override
	public void onStart() {
		super.onStart();

		if (getDialog() == null) {
			return;
		}

		if (getDialog().getWindow() == null) {
			return;
		}

		getDialog().getWindow().setWindowAnimations(
				R.style.DialogAnimation_Fade);

		getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		FragmentArgs.inject(this);
	}
}
