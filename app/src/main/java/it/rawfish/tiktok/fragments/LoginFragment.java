package it.rawfish.tiktok.fragments;


import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;

import io.jsonwebtoken.Header;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import it.rawfish.tiktok.R;
import it.rawfish.tiktok.databinding.FragmentLoginBinding;
import it.rawfish.tiktok.models.Employee;
import it.rawfish.tiktok.rest.RestBaseCallBack;
import it.rawfish.tiktok.rest.RestClient;
import it.rawfish.tiktok.utils.SharedPrefsUtils;

public class LoginFragment extends Fragment {

	private FragmentLoginBinding mBinding;
	private OnLoginListener mListener;

	public LoginFragment() {
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		mBinding = FragmentLoginBinding.inflate(inflater);

		showInputLayout();

		mBinding.userCodeText.setText("pi368");
		
		mBinding.enterBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (mBinding.userCodeText.getText().toString().isEmpty()) {
					Toast.makeText(getActivity(), R.string.user_code_no_empty, Toast.LENGTH_SHORT).show();
				} else {
					mBinding.loading.start();
					hideInputLayout();
					logUserIn();
				}
			}
		});

		mBinding.userCodeText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
				if (i == 6 && !mBinding.userCodeText.getText().toString().isEmpty()) {

					if (getActivity().getCurrentFocus() != null) {
						InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
					}

					mBinding.loading.start();
					hideInputLayout();
					logUserIn();
					return true;
				} else {
					Toast.makeText(getActivity(), R.string.user_code_no_empty, Toast.LENGTH_SHORT).show();
					return false;
				}
			}
		});

		return mBinding.getRoot();
	}

	private void logUserIn() {
		String userCode = mBinding.userCodeText.getText().toString();

		if (userCode.isEmpty())
			return;

		Map<String, Object> header = new HashMap<>(1);
		header.put("type", Header.JWT_TYPE);

		String compactJws = Jwts.builder()
				.setSubject(userCode)
				.setHeader(header)
				.signWith(SignatureAlgorithm.HS256, getString(R.string.alalala))
				.compact();

		JsonObject obj = new JsonObject();
		obj.addProperty("username", compactJws);
		RestClient.get().postUserCredentials(obj).enqueue(new RestBaseCallBack<JsonObject>() {
			@Override
			public void onSuccess(JsonObject obj) {
				if (obj.has("access_token"))
					SharedPrefsUtils.get().setInnerToken(obj.get("access_token").getAsString());

				if (obj.has("refresh_token"))
					SharedPrefsUtils.get().setRefreshToken(obj.get("refresh_token").getAsString());

				getUserData();
			}

			@Override
			public void onError(String error) {
				if (getActivity() != null) {
					mBinding.loading.stop();
					showInputLayout();
					Toast.makeText(getActivity(), "Connection error occurred", Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	private void getUserData() {
		RestClient.get().getUserData().enqueue(new RestBaseCallBack<Employee>() {
			@Override
			public void onSuccess(Employee obj) {
				Employee.getInstance().copyDataFromUser(obj);
				mBinding.loading.stop();
				Handler handler = new Handler();
				handler.postDelayed(new Runnable(){
					public void run() {
						mListener.onLoginSuccessfull();
					}
				}, 400);
			}

			@Override
			public void onError(String error) {
				if (getActivity() != null) {
					mBinding.loading.stop();
					showInputLayout();
					Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	private void showInputLayout() {
		Animation slideUp = AnimationUtils.loadAnimation(getActivity(),
				R.anim.show_slide_up);

		mBinding.inputLt.startAnimation(slideUp);
	}

	private void hideInputLayout() {
		Animation slideDown = AnimationUtils.loadAnimation(getActivity(),
				R.anim.hide_slide_down);

		mBinding.inputLt.startAnimation(slideDown);
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if (context instanceof OnLoginListener) {
			mListener = (OnLoginListener) context;
		} else {
			throw new RuntimeException(context.toString()
					+ " must implement OnLoginListener");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	public interface OnLoginListener {
		void onLoginSuccessfull();
	}
}
