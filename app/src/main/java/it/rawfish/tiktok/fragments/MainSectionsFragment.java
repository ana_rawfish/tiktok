package it.rawfish.tiktok.fragments;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;
import java.util.WeakHashMap;

import it.rawfish.tiktok.R;
import it.rawfish.tiktok.adapters.InventoriesRecyclerAdapter;
import it.rawfish.tiktok.adapters.InvoicesRecyclerAdapter;
import it.rawfish.tiktok.adapters.StoresArrayAdapter;
import it.rawfish.tiktok.databinding.FragmentMainSectionsBinding;
import it.rawfish.tiktok.fragments.generic.InventoryDetailsFragment;
import it.rawfish.tiktok.fragments.generic.InventoryDetailsFragmentBuilder;
import it.rawfish.tiktok.fragments.invoice.InvoiceDialogFragment;
import it.rawfish.tiktok.fragments.invoice.InvoiceDialogFragmentBuilder;
import it.rawfish.tiktok.models.Employee;
import it.rawfish.tiktok.models.Inventory;
import it.rawfish.tiktok.models.Invoice;
import it.rawfish.tiktok.rest.RestBaseCallBack;
import it.rawfish.tiktok.rest.RestClient;
import it.rawfish.tiktok.utils.AppUtils;
import it.rawfish.tiktok.utils.Constants;

import static android.view.View.GONE;

public class MainSectionsFragment extends BaseArgFragment implements SearchView.OnQueryTextListener, SearchView.OnCloseListener {

	private FragmentMainSectionsBinding mBinding;
	private int mFirstSectionActiveIndex = 0;
	private List<Inventory> mInventories = new ArrayList<>();
	private Long mCurrentStoreId = 0L;
	private String mCurrentSerchString = "";
	private int mCurrentInvoiceType = 0;

	private AdapterView.OnItemClickListener mSection0ItemClickListener = new AdapterView.OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
			mFirstSectionActiveIndex = i;

			mBinding.section2.list.setAdapter(new InventoriesRecyclerAdapter(
					new ArrayList<Inventory>(),
					new InventoriesRecyclerAdapter.OnItemsListListener() {
						@Override
						public void onShowDetals(Inventory inventory) {
						}
					}));

			switch (i) {
				case 0:
					getStores();
					break;
				case 1:
					setDataToSecondSection(getResources().getStringArray(R.array.inventory_parts_items));
					break;
			}

			if (mBinding.section2.getRoot().getVisibility() == View.VISIBLE) {
				mBinding.section2.getRoot().setVisibility(GONE);
			}
		}
	};

	private AdapterView.OnItemClickListener mSection1ItemClickListener = new AdapterView.OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

			switch (mFirstSectionActiveIndex) {
				case 0:
					setThirdSectionSpinner();
					mCurrentStoreId = l;
					getInvoices("", 0);
					break;
				case 1:
					setThirdSectionTitle(adapterView.getChildAt(i));
					getInventories(i);
					break;
			}

			if (mBinding.section2.getRoot().getVisibility() == GONE) {
				mBinding.section2.getRoot().setVisibility(View.VISIBLE);
			}
		}
	};

	public MainSectionsFragment() {
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		mBinding = FragmentMainSectionsBinding.inflate(inflater);

		mBinding.section0.list.setOnItemClickListener(mSection0ItemClickListener);
		mBinding.section1.list.setOnItemClickListener(mSection1ItemClickListener);
		mBinding.section2.toolbar.inflateMenu(R.menu.search_menu);
		MenuItem searchItem = mBinding.section2.toolbar.getMenu().findItem(R.id.action_search);

		if (searchItem != null) {
			SearchView searchView = (SearchView) searchItem.getActionView();
			searchView.setOnQueryTextListener(this);
			searchView.setOnCloseListener(this);
		}

		mBinding.section0.list.setAdapter(new ArrayAdapter<>(
				getActivity(),
				android.R.layout.simple_list_item_activated_1,
				getResources().getStringArray(R.array.section_0_items)
		));

		showSectionsWithAnimation();

		return mBinding.getRoot();
	}

	private void showSectionsWithAnimation() {
		Animation showSectionAnim = AnimationUtils.loadAnimation(getActivity(),
				R.anim.show_section);
		showSectionAnim.setDuration(700);

		mBinding.section0.getRoot().startAnimation(showSectionAnim);
		mBinding.section1.getRoot().startAnimation(showSectionAnim);
	}

	private void setThirdSectionTitle(View view) {
		mBinding.section2.spinnerFilter.setVisibility(View.GONE);
		TextView textView = (TextView) view.findViewById(android.R.id.text1);
		mBinding.section2.toolbar.setTitle(textView.getText());
		clearSearch();
	}

	private void setThirdSectionSpinner() {
		mBinding.section2.spinnerFilter.setVisibility(View.VISIBLE);
		mBinding.section2.toolbar.setTitle("");

		clearSearch();

		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
				R.array.invoice_types, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(R.layout.simple_dropdown_item);
		mBinding.section2.spinnerFilter.setAdapter(adapter);

		mBinding.section2.spinnerFilter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
				((TextView) mBinding.section2.spinnerFilter.getSelectedView()).setTextColor(Color.WHITE);
				mCurrentInvoiceType = i;
				getInvoices(mCurrentSerchString, i);
			}

			@Override
			public void onNothingSelected(AdapterView<?> adapterView) {

			}
		});
	}

	private void clearSearch() {
		MenuItem searchItem = mBinding.section2.toolbar.getMenu().findItem(R.id.action_search);
		if (searchItem != null) {
			SearchView searchView = (SearchView) searchItem.getActionView();
			searchView.setQuery("", false);
			searchView.clearFocus();
			mCurrentSerchString = "";
			MenuItemCompat.collapseActionView(searchItem);
		}
	}

	private void setDataToSecondSection(String[] data) {
		mBinding.section1.list.setAdapter(new ArrayAdapter<>(
				getActivity(),
				android.R.layout.simple_list_item_activated_1,
				data
		));
	}

	private void getStores() {
		mBinding.section1.list.setAdapter(new StoresArrayAdapter(
				getActivity(),
				android.R.layout.simple_list_item_activated_1,
				Employee.getInstance().getStore()));
	}

	public void getInvoices(String query, int type) {
		WeakHashMap<String, String> opts = new WeakHashMap<>();
		if (!query.isEmpty()) {
			opts.put("query", query);
		}

		if (mCurrentStoreId != 0L) {
			opts.put("store_id", String.valueOf(mCurrentStoreId));
		}

		if (type != 0) {
			opts.put("status", type == 1 ? Constants.INVOICE_STATUS_DONE : Constants.INVOICE_STATUS_PENDING);
		}

		RestClient.get().getInvoices(opts).enqueue(new RestBaseCallBack<List<Invoice>>(mBinding.section2.progress) {
			@Override
			public void onSuccess(List<Invoice> obj) {
				if (obj != null && getActivity() != null) {
					if (obj.size() == 0)
						showToast(getString(R.string.nothing_found));

					mBinding.section2.list.setAdapter(new InvoicesRecyclerAdapter(
							new InvoicesRecyclerAdapter.OnItemsListListener() {
								@Override
								public void onShowInvoiceActionsDialog(Invoice invoice) {
									showInvoiceActions(invoice);
								}
							},
							obj
					));
				}
			}

			@Override
			public void onError(String error) {
				showToast(error);
			}
		});
	}

	private void filterInventories(String text) {
		WeakHashMap<String, String> query = new WeakHashMap<>();
		query.put("item_name", text);
		RestClient.get().getInventories(query).enqueue(new RestBaseCallBack<List<Inventory>>(mBinding.section2.progress) {
			@Override
			public void onSuccess(List<Inventory> obj) {
				if (obj != null && getActivity() != null) {
					mInventories.clear();
					mInventories.addAll(obj);

					mBinding.section2.list.setAdapter(new InventoriesRecyclerAdapter(
							mInventories,
							new InventoriesRecyclerAdapter.OnItemsListListener() {
								@Override
								public void onShowDetals(Inventory inventory) {
									showInventoryDetails(inventory);
								}
							}));

					if (mInventories.size() == 0)
						showToast(getString(R.string.nothing_found));
				}
			}

			@Override
			public void onError(String error) {
				showToast(error);
			}
		});
	}

	private void getInventories(final int i) {
		RestClient.get().getInventories(new WeakHashMap<String, String>(1)).enqueue(new RestBaseCallBack<List<Inventory>>(mBinding.section2.progress) {
			@Override
			public void onSuccess(List<Inventory> obj) {
				if (obj != null && getActivity() != null) {
					mInventories.clear();
					mInventories.addAll(obj);
					setInventoriesAccordingType(i);
				}
			}

			@Override
			public void onError(String error) {
				showToast(error);
			}
		});
	}

	private void setInventoriesAccordingType(int typeIndex) {
		List<Inventory> inventories = new ArrayList<>();

		switch (typeIndex) {
			case 0:
				inventories.addAll(mInventories);
				break;
			case 1:
				inventories.addAll(AppUtils.getFilteredInventories(mInventories, Constants.SPARE_TYPE_BATTERY));
				break;
			case 2:
				inventories.addAll(AppUtils.getFilteredInventories(mInventories, Constants.SPARE_TYPE_BRACELET));
				break;
		}

		mBinding.section2.list.setAdapter(new InventoriesRecyclerAdapter(
				inventories,
				new InventoriesRecyclerAdapter.OnItemsListListener() {
					@Override
					public void onShowDetals(Inventory inventory) {
						showInventoryDetails(inventory);
					}
				}));

		if (mInventories.size() == 0)
			showToast(getString(R.string.nothing_found));
	}

	private void showInventoryDetails(Inventory inventory) {
		InventoryDetailsFragment inventoryFragment = new InventoryDetailsFragmentBuilder(inventory).build();
		inventoryFragment.show(getChildFragmentManager(), InventoryDetailsFragment.TAG);
	}

	private void showInvoiceActions(final Invoice invoice) {
		CharSequence options[];

		if (invoice.getStatus().equals(Constants.INVOICE_STATUS_PENDING)) {
			options = new CharSequence[] {
					getString(R.string.view),
					getString(R.string.send_to_customer),
					getString(R.string.edit),
					getString(R.string.back)};
		} else {
			options = new CharSequence[] {
					getString(R.string.view),
					getString(R.string.send_to_customer),
					getString(R.string.back)};
		}

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AlertDialog_Theme);
		builder.setTitle(R.string.select_option);
		builder.setItems(options, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
					case 0:
						AppUtils.openPdf(getActivity(), invoice.getPdf());
						break;
					case 1:
						RestClient.get().postInvoiceToCustomer(invoice.getId()).enqueue(new RestBaseCallBack<JsonObject>() {
							@Override
							public void onSuccess(JsonObject obj) {
								Toast.makeText(getActivity(), R.string.email_sent, Toast.LENGTH_LONG).show();
							}

							@Override
							public void onError(String error) {
								Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
							}
						});
						break;
					case 2:
						if (invoice.getStatus().equals(Constants.INVOICE_STATUS_PENDING)) {
							Invoice.setInvoice(invoice);
							new InvoiceDialogFragmentBuilder(true).build().show(getChildFragmentManager(),
									InvoiceDialogFragment.TAG);
						}
						break;
					case 3:
						break;
				}
			}
		});

		builder.show();
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		mCurrentSerchString = query;
		if (mFirstSectionActiveIndex == 0) {
			getInvoices(query, mCurrentInvoiceType);
		} else if (mFirstSectionActiveIndex == 1) {
			filterInventories(query);
		}
		
		return true;
	}

	@Override
	public boolean onQueryTextChange(String newText) {
		return false;
	}

	@Override
	public boolean onClose() {
		clearSearch();
		return true;
	}
}
