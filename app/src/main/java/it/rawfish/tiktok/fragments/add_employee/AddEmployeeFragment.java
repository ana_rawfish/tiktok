package it.rawfish.tiktok.fragments.add_employee;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import java.util.WeakHashMap;

import it.rawfish.tiktok.R;
import it.rawfish.tiktok.fragments.generic.SignatureFragment;
import it.rawfish.tiktok.fragments.generic.SignatureFragmentBuilder;
import it.rawfish.tiktok.fragments.generic.StepsDialogFragment;
import it.rawfish.tiktok.models.NewEmployee;
import it.rawfish.tiktok.utils.Constants;

public class AddEmployeeFragment extends StepsDialogFragment {
	public final static String TAG = AddEmployeeFragment.class.getSimpleName();
	private final int PAGES_COUNT = 3;

	public AddEmployeeFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		WeakHashMap<Integer, Fragment> fragmentWeakHashMap = new WeakHashMap<>(PAGES_COUNT);
		fragmentWeakHashMap.put(0, new PersonalInfoFragment());
		fragmentWeakHashMap.put(1, new SignatureFragmentBuilder(false, Constants.ROLE_EMPLOYEE).build());
		fragmentWeakHashMap.put(2, new ConfirmEmployeeFragment());

		setFragmentsIndexMap(fragmentWeakHashMap);
		setLabelsLt(R.layout.add_employee_steps_labels);
	}

	@Override
	public void onDismiss(DialogInterface dialog) {
		super.onDismiss(dialog);
		NewEmployee.getInstance().clear();
	}

	@Override
	public boolean onValidateAndStoreStepData(int step) {
		boolean isValid = true;

		if (getFragmentByIndex(step) instanceof PersonalInfoFragment) {
			isValid = ((PersonalInfoFragment) getFragmentByIndex(step)).validateAndStoreData();
		} else if (getFragmentByIndex(step) instanceof SignatureFragment) {
			isValid = ((SignatureFragment) getFragmentByIndex(step)).storeData();
			((ConfirmEmployeeFragment) getFragmentByIndex(step + 1)).refreshEmployeeData();
		} else if (getFragmentByIndex(step) instanceof ConfirmEmployeeFragment) {
			((ConfirmEmployeeFragment) getFragmentByIndex(step)).confirmEmployeeData();
		}

		return isValid;
	}
}
