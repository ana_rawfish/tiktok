package it.rawfish.tiktok.fragments.add_employee;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import it.rawfish.tiktok.databinding.FragmentEmployeeConfirmBinding;
import it.rawfish.tiktok.fragments.LoginFragment;
import it.rawfish.tiktok.fragments.generic.GenerationInProgressDialog;
import it.rawfish.tiktok.models.NewEmployee;
import it.rawfish.tiktok.utils.Constants;

public class ConfirmEmployeeFragment extends Fragment {


	private FragmentEmployeeConfirmBinding mBinding;
	private GenerationInProgressDialog.OnGenerationListener mListener;

	public ConfirmEmployeeFragment() {

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		mBinding = FragmentEmployeeConfirmBinding.inflate(inflater);

		return mBinding.getRoot();
	}

	public void confirmEmployeeData() {
		mListener.onShowGenerationDialog(Constants.ROLE_EMPLOYEE, false);
	}

	public void refreshEmployeeData() {
		if (NewEmployee.getInstance().getPhoto() != null)
			mBinding.photoView.setImageURI(Uri.parse(NewEmployee.getInstance().getPhoto()));

		if (NewEmployee.getInstance().getSignature() != null)
			mBinding.signatureView.setImageURI(Uri.parse(NewEmployee.getInstance().getSignature()));

		mBinding.name.setText(NewEmployee.getInstance().getName());
		mBinding.phone.setText(NewEmployee.getInstance().getPhone());
		mBinding.email.setText(NewEmployee.getInstance().getEmail());
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if (context instanceof LoginFragment.OnLoginListener) {
			mListener = (GenerationInProgressDialog.OnGenerationListener) context;
		} else {
			throw new RuntimeException(context.toString()
					+ " must implement OnGenerationListener");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}
}
