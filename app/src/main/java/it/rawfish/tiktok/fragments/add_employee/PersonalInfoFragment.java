package it.rawfish.tiktok.fragments.add_employee;


import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import it.rawfish.tiktok.R;
import it.rawfish.tiktok.adapters.StoresArrayAdapter;
import it.rawfish.tiktok.databinding.FragmentEmployeeInfoBinding;
import it.rawfish.tiktok.models.Employee;
import it.rawfish.tiktok.models.NewEmployee;
import it.rawfish.tiktok.models.Store;
import it.rawfish.tiktok.utils.AppUtils;
import it.rawfish.tiktok.utils.Constants;
import nl.changer.polypicker.Config;
import nl.changer.polypicker.ImagePickerActivity;

import static it.rawfish.tiktok.utils.Constants.INTENT_REQUEST_GET_IMAGES;

public class PersonalInfoFragment extends Fragment {

	private FragmentEmployeeInfoBinding mBinding;
	private Uri mPhotoUri;

	public PersonalInfoFragment() {
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		mBinding = FragmentEmployeeInfoBinding.inflate(inflater);

		mBinding.photoView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				doSelectPhoto();
			}
		});

		StoresArrayAdapter adapter = new StoresArrayAdapter(
				getActivity(),
				android.R.layout.simple_spinner_item,
				Employee.getInstance().getStore());

		adapter.setDropDownViewResource(R.layout.simple_dropdown_item);
		mBinding.storesSpinner.setAdapter(adapter);

		return mBinding.getRoot();
	}

	@Override
	public void onActivityResult(int requestCode, int resuleCode, Intent intent) {
		if (resuleCode == Activity.RESULT_OK) {
			if (requestCode == INTENT_REQUEST_GET_IMAGES) {
				Parcelable[] parcelableUris = intent.getParcelableArrayExtra(ImagePickerActivity.EXTRA_IMAGE_URIS);

				if (parcelableUris == null) {
					return;
				}

				Uri[] uris = new Uri[parcelableUris.length];
				System.arraycopy(parcelableUris, 0, uris, 0, parcelableUris.length);

				if (uris.length > 0) {
					mBinding.photoView.setImageURI(uris[0]);
					mPhotoUri = uris[0];
				}
			}
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);

		if (grantResults.length > 0) {
			if (requestCode == Constants.PERMISSION_CAMERA) {
				if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					runImageChooser();
				}
			}
		}
	}

	public void doSelectPhoto() {
		if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
				ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
			askCameraPermission();
		} else {
			runImageChooser();
		}
	}

	@TargetApi(Build.VERSION_CODES.M)
	private void askCameraPermission() {
		requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE}, Constants.PERMISSION_CAMERA);
	}

	private void runImageChooser() {
		Intent intent = new Intent(getActivity(), ImagePickerActivity.class);
		Config config = new Config.Builder()
				.setTabBackgroundColor(R.color.white)
				.setTabSelectionIndicatorColor(R.color.blue)
				.setCameraButtonColor(R.color.green)
				.setSelectionLimit(1)
				.build();
		ImagePickerActivity.setConfig(config);
		startActivityForResult(intent, INTENT_REQUEST_GET_IMAGES);
	}

	public boolean validateAndStoreData() {

		boolean isDataValid = true;
		View focusView = null;

		mBinding.name.setError(null);
		mBinding.phone.setError(null);

		String name = mBinding.name.getText().toString();
		String email = mBinding.email.getText().toString();
		String phone = mBinding.phone.getText().toString();

		Store selectedStore = Employee.getInstance().getStore().get(mBinding.storesSpinner.getSelectedItemPosition());

		if (!AppUtils.isPhoneValid(phone) || phone.isEmpty()) {
			mBinding.phone.setError(getString(R.string.phone_not_valid));
			focusView = mBinding.phone;
			isDataValid = false;
		}

		if (name.isEmpty()) {
			mBinding.name.setError(getString(R.string.name_not_empty));
			focusView = mBinding.name;
			isDataValid = false;
		}

		if (isDataValid) {
			NewEmployee.getInstance().setName(name);
			NewEmployee.getInstance().setEmail(email);
			NewEmployee.getInstance().setPhone(phone);
			NewEmployee.getInstance().setStoreID(selectedStore.getId());

			if (mPhotoUri != null)
				NewEmployee.getInstance().setPhoto(mPhotoUri.getPath());
		} else {
			focusView.requestFocus();
		}

		return isDataValid;
	}
}
