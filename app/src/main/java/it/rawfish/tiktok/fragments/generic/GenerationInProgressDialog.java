package it.rawfish.tiktok.fragments.generic;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.hannesdorfmann.fragmentargs.FragmentArgs;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import java.util.HashMap;

import it.rawfish.tiktok.R;
import it.rawfish.tiktok.databinding.FragmentGenerateBinding;
import it.rawfish.tiktok.fragments.LoginFragment;
import it.rawfish.tiktok.models.Invoice;
import it.rawfish.tiktok.models.Price;
import it.rawfish.tiktok.rest.RestBaseCallBack;
import it.rawfish.tiktok.rest.RestClient;
import it.rawfish.tiktok.services.GenerateDataIntentService;
import it.rawfish.tiktok.services.GenerateDataResultReceiver;
import it.rawfish.tiktok.utils.AppUtils;
import it.rawfish.tiktok.utils.Constants;

import static android.view.View.GONE;


@FragmentWithArgs
public class GenerationInProgressDialog extends DialogFragment {

	public static final String TAG = GenerationInProgressDialog.class.getSimpleName();

	@Arg
	String mType;

	@Arg
	boolean mIsEditMode;

	private FragmentGenerateBinding mBinding;
	private OnGenerationListener mListener;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		FragmentArgs.inject(this);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mBinding = FragmentGenerateBinding.inflate(inflater);

		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		getDialog().setCanceledOnTouchOutside(false);

		mBinding.loading.start();

		if (mIsEditMode) {
			updateInvoiceData();
		} else {
			if (mType.equals(Constants.ROLE_ADMIN)) {
				generateInvoice();
			} else if (mType.equals(Constants.ROLE_EMPLOYEE)){
				generateEmployee();
			}
		}

		return mBinding.getRoot();
	}

	@Override
	public void onStart() {
		super.onStart();

		if (getDialog() == null) {
			return;
		}

		if (getDialog().getWindow() == null) {
			return;
		}

		getDialog().getWindow().setWindowAnimations(
				R.style.DialogAnimation_Fade);
	}

	private void generateInvoice() {
		mBinding.titleMsg.setText(R.string.generating_invoice);

		Intent intent = new Intent(getActivity(), GenerateDataIntentService.class);
		intent.putExtra(Constants.ROLE, mType);
		intent.putExtra(Constants.RECEIVER, new GenerateDataResultReceiver(new Handler()) {
			@Override
			public void onReceiveResult(int resultCode, long id, final String generated) {
				Log.d("GENERATE INVOICE", "RESULT: " + resultCode);

				mBinding.loading.stop();

				if (resultCode == Constants.SUCCESS_RESULT) {
					showMessage(getString(R.string.success), R.drawable.ic_success, "");
					setSuccessInvoiceButtons(id, generated);
				} else {
					showMessage(getString(R.string.failure), R.drawable.ic_fail, getString(R.string.no_invoice));
					setInvoiceFailButtons();
				}
			}
		});

		getActivity().startService(intent);
	}

	private void showMessage(String title, int image, String message) {
		mBinding.titleMsg.setText(title);
		mBinding.resultImg.setImageDrawable(getResources().getDrawable(image));
		mBinding.resultText.setText(message);
		mBinding.resultText.setVisibility(View.VISIBLE);
	}

	private void setSuccessInvoiceButtons(final long id, final String pdfUrl) {
		dismissSuccess();
		mBinding.viewBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				AppUtils.openPdf(getActivity(), pdfUrl);
			}
		});
		mBinding.sendBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				mBinding.sendBtn.setEnabled(false);
				RestClient.get().postInvoiceToCustomer(id).enqueue(new RestBaseCallBack<JsonObject>() {
					@Override
					public void onSuccess(JsonObject obj) {
						Toast.makeText(getActivity(), R.string.email_sent, Toast.LENGTH_LONG).show();
						mBinding.sendBtn.setText(getString(R.string.sent));
						mBinding.sendBtn.setTextColor(getResources().getColor(R.color.colorConfirm));
					}

					@Override
					public void onError(String error) {
						Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
						mBinding.sendBtn.setEnabled(true);
					}
				});
			}
		});
		mBinding.deliverBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				mBinding.deliverBtn.setEnabled(false);
				HashMap<String, String> status = new HashMap<String, String>(1);
				status.put("status", Constants.INVOICE_STATUS_DONE);
				RestClient.get().updateInvoiceStatus(id, status).enqueue(new RestBaseCallBack<JsonObject>() {
					@Override
					public void onSuccess(JsonObject obj) {
						Toast.makeText(getActivity(), R.string.invoice_delivered, Toast.LENGTH_LONG).show();
						mBinding.deliverBtn.setText(getString(R.string.delivered));
						mBinding.deliverBtn.setTextColor(getResources().getColor(R.color.colorConfirm));
					}

					@Override
					public void onError(String error) {
						mBinding.deliverBtn.setEnabled(true);
						Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
					}
				});
			}
		});

		mBinding.doneBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				dismiss();
			}
		});

		mBinding.failBtnsTl.setVisibility(GONE);
		mBinding.successBtnsTl.setVisibility(View.VISIBLE);
	}

	private void setInvoiceFailButtons() {
		mBinding.backBtn.setText(getString(R.string.back_to_invoice));
		mBinding.backBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				dismiss();
			}
		});
		mBinding.cancelBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				mListener.onInvoiceGenerated();
				dismiss();
			}
		});

		mBinding.failBtnsTl.setVisibility(View.VISIBLE);
	}

	private void setEmployeeSuccessButtons() {
		mBinding.viewBtn.setVisibility(View.GONE);
		mBinding.sendBtn.setVisibility(View.GONE);
		mBinding.deliverBtn.setVisibility(View.GONE);
		mBinding.doneBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				dismiss();
			}
		});

		mBinding.failBtnsTl.setVisibility(View.GONE);
		mBinding.successBtnsTl.setVisibility(View.VISIBLE);

		dismissSuccess();
	}

	private void setEmployeeFailButtons() {
		mBinding.backBtn.setText(getString(R.string.back_to_employee));
		mBinding.backBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				dismiss();
			}
		});
		mBinding.cancelBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				mListener.onInvoiceGenerated();
				dismiss();
			}
		});

		mBinding.failBtnsTl.setVisibility(View.VISIBLE);
		mBinding.successBtnsTl.setVisibility(View.GONE);
	}

	@Override
	public void onResume() {
		super.onResume();
		Window window = getDialog().getWindow();

		if (window == null)
			return;

		int width = getResources().getDimensionPixelSize(R.dimen.dialog_width);
		int height = getResources().getDimensionPixelSize(R.dimen.dialog_height);

		window.setLayout(width, height);
		window.setGravity(Gravity.CENTER);
	}

	private void generateEmployee() {
		mBinding.titleMsg.setText(R.string.generating_employee);

		Intent intent = new Intent(getActivity(), GenerateDataIntentService.class);
		intent.putExtra(Constants.ROLE, mType);
		intent.putExtra(Constants.RECEIVER, new GenerateDataResultReceiver(new Handler()) {
			@Override
			public void onReceiveResult(int resultCode, long id, String generated) {
				Log.d("GENERATE EMPLOYEE", "RESULT: " + resultCode);

				mBinding.loading.stop();

				if (resultCode == Constants.SUCCESS_RESULT) {
					showMessage(getString(R.string.success), R.drawable.ic_success, getString(R.string.use_user_code) + generated);
					setEmployeeSuccessButtons();
				} else {
					showMessage(getString(R.string.failure), R.drawable.ic_fail, getString(R.string.no_employee_created));
					setEmployeeFailButtons();
				}
			}
		});

		getActivity().startService(intent);
	}

	private void dismissSuccess() {
		new Handler().postDelayed(new Runnable(){
			public void run() {
				if (mListener != null) {
					if (mType.equals(Constants.ROLE_ADMIN)) {
						mListener.onInvoiceGenerated();
					} else if (mType.equals(Constants.ROLE_EMPLOYEE)){
						mListener.onEmployeeGenerated();
					}
				}
			}
		}, 1000);
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if (context instanceof LoginFragment.OnLoginListener) {
			mListener = (OnGenerationListener) context;
		} else {
			throw new RuntimeException(context.toString()
					+ " must implement OnGenerationListener");
		}
	}

	private void updateInvoiceData() {
		mBinding.titleMsg.setText(R.string.generating_invoice);
		HashMap<String, Price> priceData = new HashMap<>(1);
		priceData.put("price", Invoice.get().getPrice());
		RestClient.get().updateInvoicePrice(
				Invoice.get().getId(),
				priceData).enqueue(new RestBaseCallBack<Invoice>() {
			@Override
			public void onSuccess(Invoice obj) {
				if (getActivity() != null) {
					mBinding.loading.stop();
					showMessage(getString(R.string.success), R.drawable.ic_success, "");
					setSuccessInvoiceButtons(obj.getId(), obj.getPdf());
					Toast.makeText(getActivity(), "Invoice updated", Toast.LENGTH_LONG).show();
				}
			}

			@Override
			public void onError(String error) {
				if (getActivity() != null) {
					Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
					showMessage(getString(R.string.failure), R.drawable.ic_fail, getString(R.string.no_invoice));
					setInvoiceFailButtons();
				}
			}
		});
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	public interface OnGenerationListener {
		void onShowGenerationDialog(String role, boolean isEdit);
		void onInvoiceGenerated();
		void onEmployeeGenerated();
	}
}
