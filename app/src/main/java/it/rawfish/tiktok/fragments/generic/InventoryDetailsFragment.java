package it.rawfish.tiktok.fragments.generic;


import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.hannesdorfmann.fragmentargs.bundler.ParcelerArgsBundler;

import it.rawfish.tiktok.databinding.FragmentInventoryDetailsBinding;
import it.rawfish.tiktok.fragments.BaseDialogFragment;
import it.rawfish.tiktok.models.Inventory;

@FragmentWithArgs
public class InventoryDetailsFragment extends BaseDialogFragment {

	public static final String TAG = InventoryDetailsFragment.class.getSimpleName();

	@Arg(bundler =  ParcelerArgsBundler.class)
	public Inventory mInventory;

	private FragmentInventoryDetailsBinding mBinding;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		Dialog dialog = getDialog();
		if (dialog != null) {
			assert dialog.getWindow() != null;
			dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		}

		mBinding = FragmentInventoryDetailsBinding.inflate(inflater);
		mBinding.setInventory(mInventory);
		mBinding.closeBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				dismiss();
			}
		});

		return mBinding.getRoot();
	}
}
