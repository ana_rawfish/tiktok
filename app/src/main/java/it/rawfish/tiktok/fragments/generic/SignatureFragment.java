package it.rawfish.tiktok.fragments.generic;


import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import it.rawfish.tiktok.R;
import it.rawfish.tiktok.databinding.FragmentSignatureBinding;
import it.rawfish.tiktok.fragments.BaseArgFragment;
import it.rawfish.tiktok.fragments.LoginFragment;
import it.rawfish.tiktok.models.Invoice;
import it.rawfish.tiktok.models.NewEmployee;
import it.rawfish.tiktok.utils.Constants;

@FragmentWithArgs
public class SignatureFragment extends BaseArgFragment {
	private FragmentSignatureBinding mBinding;
	private boolean bIsSigned = false;

	@Arg
	String mRole;

	private GenerationInProgressDialog.OnGenerationListener mListener;

	public SignatureFragment() {
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		mBinding = FragmentSignatureBinding.inflate(inflater);

		if (isEditMode()) {
			bIsSigned = true;
			Picasso.with(getActivity()).load(Invoice.get().getCustomer().geSignature()).into(mBinding.signatureView);
			mBinding.signatureView.setVisibility(View.VISIBLE);
			mBinding.signaturePad.setVisibility(View.GONE);
		} else {
			mBinding.signaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
				@Override
				public void onStartSigning() {

				}

				@Override
				public void onSigned() {
					bIsSigned = true;
				}

				@Override
				public void onClear() {

				}
			});
		}

		return mBinding.getRoot();
	}

	public File saveSignature(Bitmap bmp) throws IOException {
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		bmp.compress(Bitmap.CompressFormat.PNG, 30, bytes);
		File f = new File(Environment.getExternalStorageDirectory()
				+ File.separator + "signature.png");
		f.createNewFile();
		FileOutputStream fo = new FileOutputStream(f);
		fo.write(bytes.toByteArray());
		fo.close();
		return f;
	}

	public boolean storeData() {
		if (isEditMode()) {
			mListener.onShowGenerationDialog(mRole, true);
			return true;
		}

		if (bIsSigned) {
			try {
				File signature = saveSignature(mBinding.signaturePad.getTransparentSignatureBitmap(true));

				if (signature.exists()) {
					Uri uri = Uri.fromFile(signature);

					if (mRole.equals(Constants.ROLE_EMPLOYEE)) {
						NewEmployee.getInstance().setSignature(uri.getPath());
					} else if (mRole.equals(Constants.ROLE_ADMIN)){
						Invoice.get().getCustomer().setSignature(uri.getPath());
						mListener.onShowGenerationDialog(mRole, false);
					}

					return true;
				} else {
					Toast.makeText(getActivity(), R.string.error_generate_signature, Toast.LENGTH_SHORT).show();
					return false;
				}
			} catch (IOException e) {
				Toast.makeText(getActivity(), R.string.error_generate_signature, Toast.LENGTH_SHORT).show();
				e.printStackTrace();
				return false;
			}
		} else {
			Toast.makeText(getActivity(), R.string.signature_required, Toast.LENGTH_SHORT).show();
			return false;
		}
	}

	public void clearPad() {
		mBinding.signaturePad.clear();
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if (context instanceof LoginFragment.OnLoginListener) {
			mListener = (GenerationInProgressDialog.OnGenerationListener) context;
		} else {
			throw new RuntimeException(context.toString()
					+ " must implement OnGenerationListener");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}
}
