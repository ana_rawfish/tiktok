package it.rawfish.tiktok.fragments.generic;


import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.Window;

import com.badoualy.stepperindicator.StepperIndicator;

import java.util.WeakHashMap;

import it.rawfish.tiktok.R;
import it.rawfish.tiktok.databinding.FragmentStepsDialogBinding;
import it.rawfish.tiktok.fragments.BaseDialogFragment;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public abstract class StepsDialogFragment extends BaseDialogFragment {
	private int mCurrentPage = 0;
	private int mPagesCount = 0;
	private int mLabelsLt;

	private FragmentStepsDialogBinding mBinding;
	private WeakHashMap<Integer, Fragment> mFragmentsIndexMap = new WeakHashMap<>();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {

		Dialog dialog = getDialog();
		if (dialog != null)
			dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

		mBinding = FragmentStepsDialogBinding.inflate(inflater);

		mBinding.closeBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				dismiss();
			}
		});
		mBinding.nextBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				next();
			}
		});
		mBinding.backBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				back();
			}
		});
		mBinding.clearBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (mFragmentsIndexMap.get(mCurrentPage) instanceof SignatureFragment) {
					((SignatureFragment) mFragmentsIndexMap.get(mCurrentPage)).clearPad();
				}
			}
		});

		mBinding.stepIndicator.addOnStepClickListener(new StepperIndicator.OnStepClickListener() {
			@Override
			public void onStepClicked(int step) {
				if (mCurrentPage == 0) {
					onValidateAndStoreStepData(mCurrentPage);
				} else {
					mCurrentPage = step;
					mBinding.viewPager.setCurrentItem(step, true);

					if (mCurrentPage > 0)
						mBinding.backBtn.setVisibility(View.VISIBLE);

					if (mCurrentPage == mPagesCount - 1) {
						mBinding.nextBtn.setBackgroundColor(getResources().getColor(R.color.colorConfirm));
						mBinding.nextBtn.setText(getString(R.string.confirm));
						mBinding.clearBtn.setVisibility(View.VISIBLE);
					} else {
						mBinding.clearBtn.setVisibility(View.GONE);
						mBinding.nextBtn.setBackgroundColor(getResources().getColor(R.color.colorAccent));
						mBinding.nextBtn.setText(getString(R.string.next));
					}
				}
			}
		});

		mBinding.viewPager.setAdapter(new PageAdapter(getChildFragmentManager()));
		mBinding.stepIndicator.setViewPager(mBinding.viewPager);

		ViewStub stub = (ViewStub) mBinding.getRoot().findViewById(R.id.step_labels_lt);
		stub.setLayoutResource(mLabelsLt);
		stub.inflate();

		return mBinding.getRoot();
	}

	private void back() {
		if (mCurrentPage > 0) {
			--mCurrentPage;
			mBinding.viewPager.setCurrentItem(mCurrentPage, true);

			updateUI();
		}
	}

	private void  next() {
		if (mCurrentPage < mPagesCount - 1 && onValidateAndStoreStepData(mCurrentPage)) {
			++mCurrentPage;
			mBinding.viewPager.setCurrentItem(mCurrentPage, true);

			updateUI();
		} else if (mCurrentPage == mPagesCount - 1)
			onValidateAndStoreStepData(mCurrentPage);
	}

	private void updateUI() {
		if (mFragmentsIndexMap.get(mCurrentPage) instanceof SignatureFragment) {
			mBinding.clearBtn.setVisibility(VISIBLE);
		} else {
			mBinding.clearBtn.setVisibility(GONE);
		}

		if (mCurrentPage > 0)
			mBinding.backBtn.setVisibility(VISIBLE);

		if (mCurrentPage == 0)
			mBinding.backBtn.setVisibility(GONE);

		if (mCurrentPage == mPagesCount - 1) {
			mBinding.nextBtn.setBackgroundColor(getResources().getColor(R.color.colorConfirm));
			mBinding.nextBtn.setText(getString(R.string.confirm));
		} else {
			mBinding.nextBtn.setBackgroundColor(getResources().getColor(R.color.colorAccent));
			mBinding.nextBtn.setText(getString(R.string.next));
		}
	}

	public void setFragmentsIndexMap(WeakHashMap<Integer, Fragment> fragmentsMap) {
		mPagesCount = fragmentsMap.size();
		mFragmentsIndexMap.putAll(fragmentsMap);
	}

	public Fragment getFragmentByIndex(int i) {
		return mFragmentsIndexMap.get(i);
	}

	public void setLabelsLt(int mLabelsLt) {
		this.mLabelsLt = mLabelsLt;
	}

	private class PageAdapter extends FragmentStatePagerAdapter {

		public PageAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {

			return mFragmentsIndexMap.get(position);
		}

		@Override
		public int getCount() {
			return mPagesCount;
		}
	}

	public abstract boolean onValidateAndStoreStepData(int step);
}
