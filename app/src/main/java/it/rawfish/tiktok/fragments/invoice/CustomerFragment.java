package it.rawfish.tiktok.fragments.invoice;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import it.rawfish.tiktok.R;
import it.rawfish.tiktok.databinding.FragmentInvoiceCustomerBinding;
import it.rawfish.tiktok.fragments.BaseArgFragment;
import it.rawfish.tiktok.models.Customer;
import it.rawfish.tiktok.models.Invoice;
import it.rawfish.tiktok.utils.AppUtils;

@FragmentWithArgs
public class CustomerFragment extends BaseArgFragment {

	FragmentInvoiceCustomerBinding mBinding;

	public CustomerFragment() {
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		mBinding = FragmentInvoiceCustomerBinding.inflate(inflater);

		if (isEditMode())
			setCustomerData();

		return mBinding.getRoot();
	}

	private void setCustomerData() {
		if (Invoice.get().getCustomer() != null) {
			mBinding.name.setText(Invoice.get().getCustomer().getName());
			mBinding.phone.setText(Invoice.get().getCustomer().getPhone());
		}

		if (Invoice.get().getCustomer().getEmail() != null)
			mBinding.email.setText(Invoice.get().getCustomer().getEmail());

		mBinding.name.setEnabled(false);
		mBinding.phone.setEnabled(false);
		mBinding.email.setEnabled(false);
	}

	public boolean validateAndStoreData() {

		if (isEditMode())
			return true;

		boolean isDataValid = true;
		View focusView = null;

		mBinding.name.setError(null);
		mBinding.phone.setError(null);

		String name = mBinding.name.getText().toString();
		String phone = mBinding.phone.getText().toString();

		if (!AppUtils.isPhoneValid(phone) || phone.isEmpty()) {
			mBinding.phone.setError(getString(R.string.phone_not_valid));
			focusView = mBinding.phone;
			isDataValid = false;
		}

		if (name.isEmpty()) {
			mBinding.name.setError(getString(R.string.name_not_empty));
			focusView = mBinding.name;
			isDataValid = false;
		}

		if (isDataValid) {
			Invoice.get().setCustomer(new Customer(
					mBinding.name.getText().toString(),
					mBinding.email.getText().toString(),
					mBinding.phone.getText().toString()
			));
		} else {
			focusView.requestFocus();
		}

		return isDataValid;
	}
}
