package it.rawfish.tiktok.fragments.invoice;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import java.util.WeakHashMap;

import it.rawfish.tiktok.R;
import it.rawfish.tiktok.fragments.generic.SignatureFragment;
import it.rawfish.tiktok.fragments.generic.SignatureFragmentBuilder;
import it.rawfish.tiktok.fragments.generic.StepsDialogFragment;
import it.rawfish.tiktok.models.Invoice;
import it.rawfish.tiktok.models.InvoiceInitializers;
import it.rawfish.tiktok.utils.Constants;

@FragmentWithArgs
public class InvoiceDialogFragment extends StepsDialogFragment {
	public final static String TAG = InvoiceDialogFragment.class.getSimpleName();
	private final static int PAGES_COUNT = 7;

	@Arg
	public boolean mIsEditMode;

	public InvoiceDialogFragment() {
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		WeakHashMap<Integer, Fragment> fragmentWeakHashMap = new WeakHashMap<>(PAGES_COUNT);
		fragmentWeakHashMap.put(0, new CustomerFragmentBuilder(isEditMode()).build());
		fragmentWeakHashMap.put(1, new ProductFragmentBuilder(isEditMode()).build());
		fragmentWeakHashMap.put(2, new PhotoFragmentBuilder(isEditMode()).build());
		fragmentWeakHashMap.put(3, new ServiceFragmentBuilder(isEditMode()).build());
		fragmentWeakHashMap.put(4, new PriceFragmentBuilder(isEditMode()).build());
		fragmentWeakHashMap.put(5, new ReviewFragmentBuilder(isEditMode()).build());
		fragmentWeakHashMap.put(6, new SignatureFragmentBuilder(isEditMode() , Constants.ROLE_ADMIN).build());

		setFragmentsIndexMap(fragmentWeakHashMap);
		setLabelsLt(R.layout.invoice_step_labels);
	}

	@Override
	public void onDismiss(DialogInterface dialog) {
		super.onDismiss(dialog);
		Invoice.get().clear();
		InvoiceInitializers.get().clear();
	}

	@Override
	public boolean onValidateAndStoreStepData(int step) {
		boolean isValid = true;

		if (getFragmentByIndex(step) instanceof CustomerFragment) {
			isValid = ((CustomerFragment) getFragmentByIndex(step)).validateAndStoreData();
		} else if (getFragmentByIndex(step)instanceof ProductFragment) {
			((ProductFragment) getFragmentByIndex(step)).storeData();
		} else if (getFragmentByIndex(step) instanceof PhotoFragment) {
			((PhotoFragment) getFragmentByIndex(step)).storeData();
		} else if (getFragmentByIndex(step) instanceof ServiceFragment) {
			((ServiceFragment) getFragmentByIndex(step)).storeData();
			((PriceFragment) getFragmentByIndex(step + 1)).refreshPriceData();
		} else if (getFragmentByIndex(step) instanceof PriceFragment) {
			((PriceFragment) getFragmentByIndex(step)).storePriceData();
			((ReviewFragment) getFragmentByIndex(step + 1)).refreshInvoiceData();
		} else if (getFragmentByIndex(step) instanceof SignatureFragment) {
			isValid = ((SignatureFragment) getFragmentByIndex(step)).storeData();
		}

		return isValid;
	}

	public boolean isEditMode() {
		return mIsEditMode;
	}

	public void setIsEditMode(boolean mIsEditMode) {
		this.mIsEditMode = mIsEditMode;
	}
}
