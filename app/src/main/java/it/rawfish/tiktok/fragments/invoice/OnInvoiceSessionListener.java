package it.rawfish.tiktok.fragments.invoice;


public interface OnInvoiceSessionListener {
	void onCompleteInvoiceSession();
}
