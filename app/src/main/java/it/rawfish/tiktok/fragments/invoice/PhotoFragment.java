package it.rawfish.tiktok.fragments.invoice;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import java.util.ArrayList;

import it.rawfish.tiktok.R;
import it.rawfish.tiktok.adapters.GridImagesAdapter;
import it.rawfish.tiktok.databinding.FragmentInvoicePhotoBinding;
import it.rawfish.tiktok.fragments.BaseArgFragment;
import it.rawfish.tiktok.models.Invoice;
import it.rawfish.tiktok.models.Photo;
import it.rawfish.tiktok.utils.Binder;
import it.rawfish.tiktok.utils.Constants;
import nl.changer.polypicker.Config;
import nl.changer.polypicker.ImagePickerActivity;

import static it.rawfish.tiktok.utils.Constants.INTENT_REQUEST_GET_IMAGES;

@FragmentWithArgs
public class PhotoFragment extends BaseArgFragment {

	private FragmentInvoicePhotoBinding mBinding;
	private GridImagesAdapter mPhotosAdapter;

	public PhotoFragment() {
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		mBinding = FragmentInvoicePhotoBinding.inflate(inflater);

		mBinding.previewGrid.setLayoutManager(new GridLayoutManager(getActivity(), 2));

		if (isEditMode() && Invoice.get().getProduct() != null) {
			if (Invoice.get().getProduct().getPhotos() != null) {
				if (Invoice.get().getProduct().getPhotos().size() > 0) {
					mBinding.ph.setVisibility(View.GONE);
					Binder.bindPhotosGrid(mBinding.previewGrid, Invoice.get().getProduct().getPhotos(), isEditMode(), false);
				}
			}

			if (Invoice.get().getProduct().getNotes() != null)
				mBinding.notes.setText(Invoice.get().getProduct().getNotes());

			mBinding.notes.setEnabled(false);
		} else {
			mBinding.ph.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					doSelectPhoto();
				}
			});
		}
		
		return mBinding.getRoot();
	}

	@Override
	public void onActivityResult(int requestCode, int resuleCode, Intent intent) {
		if (resuleCode == Activity.RESULT_OK) {
			if (requestCode == INTENT_REQUEST_GET_IMAGES) {
				Parcelable[] parcelableUris = intent.getParcelableArrayExtra(ImagePickerActivity.EXTRA_IMAGE_URIS);

				if (parcelableUris == null) {
					return;
				}

				Uri[] uris = new Uri[parcelableUris.length];
				System.arraycopy(parcelableUris, 0, uris, 0, parcelableUris.length);

				ArrayList<String> pathsArray = new ArrayList<>(uris.length);
				for (int i = 0; i < uris.length; ++i) {
					pathsArray.add(uris[i].getPath());
				}

				if (uris.length > 0) {
					mBinding.ph.setVisibility(View.GONE);

					if (mPhotosAdapter == null) {
						mPhotosAdapter = new GridImagesAdapter(
								pathsArray,
								false,
								true,
								new GridImagesAdapter.OnPhotosAdapterListener() {
							@Override
							public void onChooseMorePhotos() {
								runImageChooser();
							}
						});

						mBinding.previewGrid.setAdapter(mPhotosAdapter);
					} else {
						mPhotosAdapter.addItems(pathsArray);
					}
				}
			}
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);

		if (grantResults.length > 0) {
			if (requestCode == Constants.PERMISSION_CAMERA ) {
				if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					Log.d("Perm", "Wooohoo camera permission granted!");
					runImageChooser();
				}
			}
		}
	}

	public void doSelectPhoto() {
		if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
				ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
			askCameraPermission();
		} else {
			runImageChooser();
		}
	}

	@TargetApi(Build.VERSION_CODES.M)
	private void askCameraPermission() {
		requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE}, Constants.PERMISSION_CAMERA);
	}

	private void runImageChooser() {
		Intent intent = new Intent(getActivity(), ImagePickerActivity.class);
		Config config = new Config.Builder()
				.setTabBackgroundColor(R.color.white)
				.setTabSelectionIndicatorColor(R.color.blue)
				.setCameraButtonColor(R.color.green)
				.setSelectionLimit(3)
				.build();
		ImagePickerActivity.setConfig(config);
		startActivityForResult(intent, INTENT_REQUEST_GET_IMAGES);
	}

	public void storeData() {
		if (isEditMode())
			return;

		if (mPhotosAdapter != null) {
			ArrayList<Photo> photos = new ArrayList<>();
			for (String path : mPhotosAdapter.getPaths()) {
				photos.add(new Photo(path));
			}

			Invoice.get().getProduct().setPhotos(photos);
		}

		Invoice.get().getProduct().setNotes(mBinding.notes.getText().toString());
	}
}
