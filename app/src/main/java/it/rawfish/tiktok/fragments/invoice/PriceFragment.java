package it.rawfish.tiktok.fragments.invoice;


import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import java.util.ArrayList;

import it.rawfish.tiktok.databinding.FragmentInvoicePriceBinding;
import it.rawfish.tiktok.databinding.ItemPartBinding;
import it.rawfish.tiktok.fragments.BaseArgFragment;
import it.rawfish.tiktok.models.Invoice;
import it.rawfish.tiktok.models.Part;
import it.rawfish.tiktok.models.Price;
import it.rawfish.tiktok.utils.AppUtils;
import it.rawfish.tiktok.utils.Constants;

import static it.rawfish.tiktok.utils.AppUtils.convertLongPriceToString;
import static it.rawfish.tiktok.utils.AppUtils.convertStringPriceToLong;

@FragmentWithArgs
public class PriceFragment extends BaseArgFragment {

	private FragmentInvoicePriceBinding mBinding;
	private ArrayList<TextInputEditText> mPartsTvArray;

	public PriceFragment() {
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		mBinding = FragmentInvoicePriceBinding.inflate(inflater);

		if (isEditMode())
			refreshPriceData();

		return mBinding.getRoot();
	}

	public void refreshPriceData() {
		mBinding.servicePrice.addTextChangedListener(new PriceTextWatcher(mBinding.servicePrice));
		mBinding.batteryPrice.addTextChangedListener(new PriceTextWatcher(mBinding.batteryPrice));
		mBinding.braceletPrice.addTextChangedListener(new PriceTextWatcher(mBinding.braceletPrice));
		mBinding.deposit.addTextChangedListener(new PriceTextWatcher(mBinding.deposit));

		refreshPartsData();

		if (isEditMode()) {
			prefillPrices();
		} else {
			refreshInventoriesData();
		}
	}

	private void prefillPrices() {
		if (Invoice.get().getPrice() != null) {
			mBinding.batteryPrice.setText(Invoice.get().getPrice().getFormattedBatteryPrice());
			mBinding.totalPrice.setText(Invoice.get().getPrice().getFormattedTotalPrice());
			mBinding.braceletPrice.setText(Invoice.get().getPrice().getFormattedBraceletPrice());
			mBinding.deposit.setText(Invoice.get().getPrice().getFormattedDeposit());
			mBinding.payOnDelivery.setText(Invoice.get().getPrice().getFormattedPayOnDeliveryPrice());
			mBinding.servicePrice.setText(Invoice.get().getPrice().getFormattedServicePrice());
		}
	}

	private void refreshInventoriesData() {
		if (Invoice.get().getService().getSelectedInventory(Constants.SPARE_TYPE_BATTERY) != null) {
			mBinding.batteryPrice.setText(AppUtils.convertLongPriceToString(
					Invoice.get().getService().getSelectedInventory(Constants.SPARE_TYPE_BATTERY).getPrice()
			));
		}

		if (Invoice.get().getService().getSelectedInventory(Constants.SPARE_TYPE_BRACELET) != null) {
			mBinding.braceletPrice.setText(AppUtils.convertLongPriceToString(
					Invoice.get().getService().getSelectedInventory(Constants.SPARE_TYPE_BRACELET).getPrice()
			));
		}
	}

	private void formatAndRecalculate(TextInputEditText editText, String string) {
		String result = convertLongPriceToString(convertStringPriceToLong((string)));
		editText.setText(result);
		Editable editobj = editText.getText();
		Selection.setSelection(editobj, result.length() - 3);
		recalculateTotalPrice();
	}

	private void recalculateTotalPrice() {
		long batteryPrice = convertStringPriceToLong(mBinding.batteryPrice.getText().toString());
		long braceletPrice = convertStringPriceToLong(mBinding.braceletPrice.getText().toString());
		long servicePrice = convertStringPriceToLong(mBinding.servicePrice.getText().toString());
		long depositPrice = convertStringPriceToLong(mBinding.deposit.getText().toString());

		long totalPartsPrice = 0;
		if (mPartsTvArray != null) {
			for (TextInputEditText textInputEditText : mPartsTvArray) {
				totalPartsPrice = totalPartsPrice + convertStringPriceToLong(textInputEditText.getText().toString());
			}
		}

		long total = batteryPrice + braceletPrice + servicePrice + totalPartsPrice;

		mBinding.totalPrice.setText(convertLongPriceToString(total));
		mBinding.payOnDelivery.setText(convertLongPriceToString(total - depositPrice));
	}

	public void storePriceData() {
		setPartsPrices();

		long totalPartsPrice = 0;
		if (Invoice.get().getService() != null && Invoice.get().getService().getParts() != null) {
			for (Part part : Invoice.get().getService().getParts()) {
				totalPartsPrice = totalPartsPrice + part.getPrice();
			}
		}
		Invoice.get().setPrice(new Price(
				convertStringPriceToLong(mBinding.batteryPrice.getText().toString()),
				convertStringPriceToLong(mBinding.totalPrice.getText().toString()),
				convertStringPriceToLong(mBinding.braceletPrice.getText().toString()),
				convertStringPriceToLong(mBinding.deposit.getText().toString()),
				convertStringPriceToLong(mBinding.payOnDelivery.getText().toString()),
				convertStringPriceToLong(mBinding.servicePrice.getText().toString()),
				totalPartsPrice
		));

	}

	public void setPartsPrices() {
		if (Invoice.get().getService() != null) {
			if (Invoice.get().getService().getParts() != null) {
				for (int i = 0; i < Invoice.get().getService().getParts().size(); ++i) {
					Part part = Invoice.get().getService().getParts().get(i);
					part.setPrice(convertStringPriceToLong(mPartsTvArray.get(i).getText().toString()));
				}
			}
		}
	}

	public void refreshPartsData() {
		if (Invoice.get().getService() != null) {
			if (mBinding.partsLt.getChildCount() > 0) {
				mBinding.partsLt.removeAllViews();
				mPartsTvArray.clear();
			}

			for (Part part : Invoice.get().getService().getParts()) {
				if (mPartsTvArray == null)
					mPartsTvArray = new ArrayList<>();

				ItemPartBinding binding = ItemPartBinding.inflate(LayoutInflater.from(getActivity()));
				binding.setPart(part);
				binding.setIsPriceEditable(true);
				binding.priceField.addTextChangedListener(new PriceTextWatcher(binding.priceField));

				mBinding.partsLt.addView(binding.getRoot());
				mPartsTvArray.add(binding.priceField);
			}
		}
	}

	private class PriceTextWatcher implements TextWatcher {
		TextInputEditText mEditText;

		PriceTextWatcher(TextInputEditText mEditText) {
			this.mEditText = mEditText;
		}

		@Override
		public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

		}

		@Override
		public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
			mEditText.removeTextChangedListener(this);
			formatAndRecalculate(mEditText, charSequence.toString());
			mEditText.addTextChangedListener(this);
		}

		@Override
		public void afterTextChanged(Editable editable) {

		}
	}
}
