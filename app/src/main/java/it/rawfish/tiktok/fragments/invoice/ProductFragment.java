package it.rawfish.tiktok.fragments.invoice;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import java.util.ArrayList;

import it.rawfish.tiktok.R;
import it.rawfish.tiktok.databinding.FragmentInvoiceProductBinding;
import it.rawfish.tiktok.fragments.BaseArgFragment;
import it.rawfish.tiktok.models.Invoice;
import it.rawfish.tiktok.models.InvoiceInitializers;
import it.rawfish.tiktok.models.Product;

import static it.rawfish.tiktok.R.string.gender;

@FragmentWithArgs
public class ProductFragment extends BaseArgFragment {

	FragmentInvoiceProductBinding mBinding;

	public ProductFragment() {
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		mBinding = FragmentInvoiceProductBinding.inflate(inflater);

		if (isEditMode())
			setProductData();
		else
			initProductData();

		return mBinding.getRoot();
	}

	private void setProductData() {
		if (Invoice.get().getProduct() != null) {

			if (Invoice.get().getProduct().getBrand() != null)
				mBinding.brand.setText(Invoice.get().getProduct().getBrand());

			if (Invoice.get().getProduct().getBracelet() != null)
				mBinding.bracelet.setText(Invoice.get().getProduct().getBracelet());

			if (Invoice.get().getProduct().getModel() != null)
				mBinding.model.setText(Invoice.get().getProduct().getModel());

			if (Invoice.get().getProduct().getSerial_number() != null)
				mBinding.serialNumber.setText(Invoice.get().getProduct().getSerial_number());

			if (Invoice.get().getProduct().getPlat() != null)
				mBinding.plat.setText(Invoice.get().getProduct().getPlat());

			if (Invoice.get().getProduct().getType() != null)
				mBinding.type.setText(Invoice.get().getProduct().getType());

			if (Invoice.get().getProduct().getGender() != null) {
				ArrayList<String> gender = new ArrayList<String>();
				gender.add(Invoice.get().getProduct().getGender());
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
						R.layout.simple_dropdown_item, gender);
				mBinding.genderSpinner.setAdapter(adapter);
			}

			mBinding.brand.setEnabled(false);
			mBinding.bracelet.setEnabled(false);
			mBinding.model.setEnabled(false);
			mBinding.serialNumber.setEnabled(false);
			mBinding.plat.setEnabled(false);
			mBinding.type.setEnabled(false);
			mBinding.genderSpinner.setEnabled(false);
		}
	}

	private void initProductData() {
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
				R.array.gender_array, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(R.layout.simple_dropdown_item);
		mBinding.genderSpinner.setAdapter(adapter);

		if (InvoiceInitializers.get().getBrands() != null) {
			setAutocompleteAdapter(mBinding.brand, InvoiceInitializers.get().getBrands());
		}

		if (InvoiceInitializers.get().getBracelets() != null) {
			setAutocompleteAdapter(mBinding.bracelet, InvoiceInitializers.get().getBracelets());
		}

		if (InvoiceInitializers.get().getPlates() != null) {
			setAutocompleteAdapter(mBinding.plat, InvoiceInitializers.get().getPlates());
		}

		if (InvoiceInitializers.get().getWatch_types() != null) {
			setAutocompleteAdapter(mBinding.type, InvoiceInitializers.get().getWatch_types());
		}
	}

	private void setAutocompleteAdapter(AutoCompleteTextView tv, ArrayList<String> array) {
		if (array.size() > 0) {
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
					R.layout.simple_dropdown_item, array);
			tv.setAdapter(adapter);
		}
	}

	public void storeData() {
		if (isEditMode())
			return;

		String gender = "";

		if (mBinding.genderSpinner.getSelectedItem() != null) {
			gender = mBinding.genderSpinner.getSelectedItemPosition() == 0 ? "" :
					mBinding.genderSpinner.getSelectedItem().toString();
		}

		Invoice.get().setProduct(new Product(
				mBinding.brand.getText().toString(),
				mBinding.bracelet.getText().toString(),
				mBinding.model.getText().toString(),
				mBinding.serialNumber.getText().toString(),
				mBinding.plat.getText().toString(),
				mBinding.type.getText().toString(),
				gender,
				""
		));
	}
}
