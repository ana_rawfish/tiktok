package it.rawfish.tiktok.fragments.invoice;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import it.rawfish.tiktok.databinding.FragmentInvoiceReviewBinding;
import it.rawfish.tiktok.fragments.BaseArgFragment;
import it.rawfish.tiktok.models.Invoice;
import it.rawfish.tiktok.utils.Binder;

@FragmentWithArgs
public class ReviewFragment extends BaseArgFragment {

	private FragmentInvoiceReviewBinding mBinding;

	public ReviewFragment() {

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		mBinding = FragmentInvoiceReviewBinding.inflate(inflater);

		if (Invoice.get().getProduct() != null) {
			if (Invoice.get().getProduct().getPhotos() != null) {
				Binder.bindPhotosGrid(mBinding.photosGrid, Invoice.get().getProduct().getPhotos(), isEditMode(), false);
				mBinding.ph.setVisibility(Invoice.get().getProduct().getPhotos().size() > 0 ? View.GONE : View.VISIBLE);
			}
		}

		return mBinding.getRoot();
	}

	public void refreshInvoiceData() {
		mBinding.setInvoice(Invoice.get());
	}
}
