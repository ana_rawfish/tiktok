package it.rawfish.tiktok.fragments.invoice;


import android.app.DatePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;

import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.rey.material.widget.Spinner;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.WeakHashMap;

import it.rawfish.tiktok.R;
import it.rawfish.tiktok.adapters.InventoriesDropdownAdapter;
import it.rawfish.tiktok.adapters.PartsAutocompleteArrayAdapter;
import it.rawfish.tiktok.databinding.FragmentInvoiceServiceBinding;
import it.rawfish.tiktok.fragments.BaseArgFragment;
import it.rawfish.tiktok.models.Inventory;
import it.rawfish.tiktok.models.Invoice;
import it.rawfish.tiktok.models.InvoiceInitializers;
import it.rawfish.tiktok.models.Part;
import it.rawfish.tiktok.models.Service;
import it.rawfish.tiktok.rest.RestBaseCallBack;
import it.rawfish.tiktok.rest.RestClient;
import it.rawfish.tiktok.utils.AppUtils;
import it.rawfish.tiktok.utils.Constants;

@FragmentWithArgs
public class ServiceFragment extends BaseArgFragment {
	private final String DATE_FORMAT = "dd/MM/yyyy";
	private FragmentInvoiceServiceBinding mBinding;
	private Calendar myCalendar = Calendar.getInstance();
	private ArrayList<Part> mPartsList = new ArrayList<>();
	private Inventory mBattery;
	private Inventory mBracelet;

	private DatePickerDialog.OnDateSetListener mDateSelectedListener = new DatePickerDialog.OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
		                      int dayOfMonth) {
			myCalendar.set(Calendar.YEAR, year);
			myCalendar.set(Calendar.MONTH, monthOfYear);
			myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
			updateDateLabel();
		}

	};

	public ServiceFragment() {
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		mBinding = FragmentInvoiceServiceBinding.inflate(inflater);

		if (isEditMode()) {
			prefillServiceData();
		} else {
			initServiceData();
		}

		return mBinding.getRoot();
	}

	private void prefillServiceData() {
		if (Invoice.get().getService() != null) {
			if (Invoice.get().getService().getBriefing() != null)
				mBinding.briefing.setText(Invoice.get().getService().getBriefing());

			if (Invoice.get().getService().getActivity() != null)
				mBinding.activity.setText(Invoice.get().getService().getActivity());

			if (Invoice.get().getService().getDetails() != null)
				mBinding.details.setText(Invoice.get().getService().getDetails());

			if (Invoice.get().getService().getDelivery() != null)
				mBinding.deliveryDate.setText(Invoice.get().getService().getDelivery());

			if (Invoice.get().getService().getParts() != null) {
				for (int i = 0; i < Invoice.get().getService().getParts().size(); ++i) {
					String partName = Invoice.get().getService().getParts().get(i).getName();
					if (i == 0)
						mBinding.parts.setText(mBinding.parts.getText().append(partName));
					else
						mBinding.parts.setText(mBinding.parts.getText().append(" ," + partName));
				}
			}

			if (Invoice.get().getService().getGuarantee() != null) {
				ArrayList<String> warr = new ArrayList<>(1);
				warr.add(Invoice.get().getService().getGuarantee());
				setWarrantySpinnerValues(warr);
			}
		}

		mBinding.briefing.setEnabled(false);
		mBinding.activity.setEnabled(false);
		mBinding.details.setEnabled(false);
		mBinding.deliveryDate.setEnabled(false);
		mBinding.parts.setEnabled(false);
		mBinding.warrantySpinner.setEnabled(false);
	}

	private void initServiceData() {
		setPartsAutocomplete();
		setWarrantySpinnerValues(AppUtils.getWarrantyMonths(getString(R.string.month)));
		setInventoriesSpinnerValues();

		mBinding.deliveryDate.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				new DatePickerDialog(getActivity(), mDateSelectedListener, myCalendar
						.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
						myCalendar.get(Calendar.DAY_OF_MONTH)).show();
			}
		});
	}

	private void setWarrantySpinnerValues(ArrayList<String> values) {
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(
				getActivity(),
				android.R.layout.simple_spinner_item,
				values
		);

		adapter.setDropDownViewResource(R.layout.simple_dropdown_item);
		mBinding.warrantySpinner.setAdapter(adapter);
	}

	private void setPartsAutocomplete() {
		mBinding.parts.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				String text = mBinding.parts.getText().toString();

				if (!text.isEmpty()) {

					if (!InvoiceInitializers.get().isAlreadyStoredPart(text))
						mPartsList.add(new Part(text));

					mBinding.partsNamesText.setText(
							mBinding.partsNamesText.getText().toString().isEmpty() ?
									text : mBinding.partsNamesText.getText() + ", " + text
					);

					mBinding.parts.setText("");
				}
			}
		});

		mBinding.parts.setAdapter(new PartsAutocompleteArrayAdapter(
				getActivity(),
				R.layout.simple_dropdown_item,
				InvoiceInitializers.get().getParts()));

		mBinding.parts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				mPartsList.add((Part)adapterView.getItemAtPosition(i));
			}
		});
	}

	private void setInventoriesSpinnerValues() {
		RestClient.get().getInventories(new WeakHashMap<String, String>(1)).enqueue(new RestBaseCallBack<List<Inventory>>() {
			@Override
			public void onSuccess(List<Inventory> obj) {
				if (obj != null && getActivity() != null) {
					mBinding.batteriesSpinner.setAdapter(new InventoriesDropdownAdapter(
							getString(R.string.not_selected),
							AppUtils.getFilteredInventories(obj, Constants.SPARE_TYPE_BATTERY)
					));

					mBinding.braceletsSpinner.setAdapter(new InventoriesDropdownAdapter(
							getString(R.string.not_selected),
							AppUtils.getFilteredInventories(obj, Constants.SPARE_TYPE_BRACELET)
					));
				}
			}

			@Override
			public void onError(String error) {
				Log.e("INV", "error: " + error);
			}
		});

		mBinding.batteriesSpinner.setOnItemClickListener(new Spinner.OnItemClickListener() {
			@Override
			public boolean onItemClick(Spinner parent, View view, int position, long id) {
				mBattery = (Inventory) parent.getAdapter().getItem(position);
				return true;
			}
		});

		mBinding.braceletsSpinner.setOnItemClickListener(new Spinner.OnItemClickListener() {
			@Override
			public boolean onItemClick(Spinner parent, View view, int position, long id) {
				mBracelet = (Inventory) parent.getAdapter().getItem(position);
				return true;
			}
		});
	}

	private void updateDateLabel() {
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT, Locale.US);
		mBinding.deliveryDate.setText(sdf.format(myCalendar.getTime()));
	}

	public void storeData() {
		if (isEditMode())
			return;

		ArrayList<Inventory> allInventories = new ArrayList<>();
		if (mBattery != null)
			allInventories.add(mBattery);
		if (mBracelet != null)
			allInventories.add(mBracelet);

		String warranty = "";
		if (mBinding.warrantySpinner.getSelectedItem() != null)
			warranty = mBinding.warrantySpinner.getSelectedItem().toString();

		Invoice.get().setService(new Service(
				mBinding.briefing.getText().toString(),
				mBinding.activity.getText().toString(),
				mBinding.details.getText().toString(),
				mBinding.deliveryDate.getText().toString(),
				warranty,
				allInventories,
				mPartsList
		));
	}
}
