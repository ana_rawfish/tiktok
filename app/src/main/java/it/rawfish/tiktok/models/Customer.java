package it.rawfish.tiktok.models;

public class Customer {
	String name;
	String email;
	String phone;
	String signature;

	public Customer() {
	}

	public Customer(String name, String email, String phone) {
		this.name = name;
		this.email = email;
		this.phone = phone;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String geSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}
}
