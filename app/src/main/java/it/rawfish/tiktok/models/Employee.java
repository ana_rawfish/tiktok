package it.rawfish.tiktok.models;


import java.util.ArrayList;
import java.util.List;

public class Employee {
	long id;

	String username;
	String phone;
	String email;
	String photo;
	String signature;
	String role;
	String name;

	List<Store> store;

	private static Employee mInstance;

	protected Employee() {
	}

	public static Employee getInstance() {
		if (mInstance == null) {
			mInstance = new Employee();
		}

		return mInstance;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public List<Store> getStore() {
		return store;
	}

	public void setStore(List<Store> store) {
		this.store = store;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public void setUserData(long id, String name, String phone, String email, String photo, String signature, List<Store> stores) {
		this.id = id;
		this.username = name;
		this.phone = phone;
		this.email = email;
		this.photo = photo;
		this.signature = signature;
		this.store = stores;
	}

	public void copyDataFromUser(Employee employee) {
		this.id = employee.id;
		this.username = employee.username;
		this.phone = employee.phone;
		this.email = employee.email;
		this.photo = employee.photo;
		this.signature = employee.signature;
		this.store = new ArrayList<>();
		this.store.addAll(employee.store);
		this.role = employee.role;
		this.name = employee.name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void clear() {
		this.id = 0;
		this.username = null;
		this.phone = null;
		this.email = null;
		this.photo = null;
		this.signature = null;
		this.role = null;
		this.store = null;
		this.name = null;
	}
}
