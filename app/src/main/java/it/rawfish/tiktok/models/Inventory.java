package it.rawfish.tiktok.models;

import org.parceler.Parcel;

@Parcel
public class Inventory {
	long id;
	long quantity;
	long price;

	String code;
	String serial_number;
	String photo;
	String name;
	String size;
	String item_type;
	String color;
	String brand;
	String additional_information;

	boolean availability;

	public Inventory() {
	}

	public Inventory(String name) {
		this.name = name;
	}

	public Inventory(long id, long quantity, long price, String code, String serial_number, String photo, String name, String size, String item_type, String color, String brand, String additional_information, boolean availability) {
		this.id = id;
		this.quantity = quantity;
		this.price = price;
		this.code = code;
		this.serial_number = serial_number;
		this.photo = photo;
		this.name = name;
		this.size = size;
		this.item_type = item_type;
		this.color = color;
		this.brand = brand;
		this.additional_information = additional_information;
		this.availability = availability;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getQuantity() {
		return quantity;
	}

	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}

	public long getPrice() {
		return price;
	}

	public void setPrice(long price) {
		this.price = price;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSerial_number() {
		return serial_number;
	}

	public void setSerial_number(String serial_number) {
		this.serial_number = serial_number;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getItem_type() {
		return item_type;
	}

	public void setItem_type(String item_type) {
		this.item_type = item_type;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getAdditional_info() {
		return additional_information;
	}

	public void setAdditional_information(String additional_information) {
		this.additional_information = additional_information;
	}

	public boolean isAvailability() {
		return availability;
	}

	public void setAvailability(boolean availability) {
		this.availability = availability;
	}
}
