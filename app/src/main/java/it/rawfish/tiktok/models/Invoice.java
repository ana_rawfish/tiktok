package it.rawfish.tiktok.models;

public class Invoice {
	long id;

	String invoice_number;
	String created_at;
	String type;
	String pdf;
	String status;

	Customer customer;
	Product product;
	Service service;
	Price price;

	private static Invoice mInstance;

	protected Invoice() {
	}

	public static Invoice get() {
		if (mInstance == null) {
			mInstance = new Invoice();
		}

		return mInstance;
	}

	public static void setInvoice(Invoice invoice) {
		mInstance = invoice;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getInvoice_number() {
		return invoice_number;
	}

	public void setInvoice_number(String invoice_number) {
		this.invoice_number = invoice_number;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}

	public Price getPrice() {
		return price;
	}

	public void setPrice(Price price) {
		this.price = price;
	}

	public String getPdf() {
		return pdf;
	}

	public void setPdf(String pdf) {
		this.pdf = pdf;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void clear() {

		id = 0;

		invoice_number = null;
		created_at = null;
		type = null;
		pdf = null;
		status = null;

		customer = null;
		product = null;
		service = null;
		price = null;

		mInstance = null;
	}
}
