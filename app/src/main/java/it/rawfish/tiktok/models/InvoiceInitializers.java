package it.rawfish.tiktok.models;


import java.util.ArrayList;

public class InvoiceInitializers {
	ArrayList<String> bracelets;
	ArrayList<String> brands;
	ArrayList<String> plates;
	ArrayList<Part> parts;
	ArrayList<String> watch_types;

	private static InvoiceInitializers mInstance;

	protected InvoiceInitializers() {
	}

	public static InvoiceInitializers get() {
		if (mInstance == null) {
			mInstance = new InvoiceInitializers();
		}

		return mInstance;
	}

	public void setData(InvoiceInitializers data) {
		this.bracelets = data.bracelets;
		this.brands = data.brands;
		this.plates = data.plates;
		this.parts = data.parts;
		this.watch_types = data.watch_types;
	}

	public ArrayList<String> getBracelets() {
		return bracelets;
	}

	public void setBracelets(ArrayList<String> bracelets) {
		this.bracelets = bracelets;
	}

	public ArrayList<String> getBrands() {
		return brands;
	}

	public void setBrands(ArrayList<String> brands) {
		this.brands = brands;
	}

	public ArrayList<String> getPlates() {
		return plates;
	}

	public void setPlates(ArrayList<String> plates) {
		this.plates = plates;
	}

	public ArrayList<Part> getParts() {
		return parts;
	}

	public void setParts(ArrayList<Part> parts) {
		this.parts = parts;
	}

	public ArrayList<String> getWatch_types() {
		return watch_types;
	}

	public void setWatch_types(ArrayList<String> watch_types) {
		this.watch_types = watch_types;
	}

	public ArrayList<String> getPartsNames() {
		ArrayList<String> names = new ArrayList<>();
		for (Part part : this.parts) {
			names.add(part.getName());
		}
		return names;
	}

	public long getPriceByName(String name) {
		for (Part part : this.parts) {
			if (part.name.equals(name))
				return part.price;
		}

		return 0;
	}

	public boolean isAlreadyStoredPart(String name) {
		for (Part part : this.parts) {
			if (part.name.equals(name))
				return true;
		}

		return false;
	}

	public void clear() {
		bracelets = null;
		brands = null;
		plates = null;
		parts = null;
		watch_types = null;
	}
}
