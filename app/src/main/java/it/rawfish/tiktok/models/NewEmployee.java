package it.rawfish.tiktok.models;


public class NewEmployee {
	String name;
	String phone;
	String email;

	long storeID;

	String photo;
	String signature;

	private static NewEmployee mInstance;

	protected NewEmployee() {
	}

	public static NewEmployee getInstance() {
		if (mInstance == null) {
			mInstance = new NewEmployee();
		}

		return mInstance;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public long getStoreID() {
		return storeID;
	}

	public void setStoreID(long storeID) {
		this.storeID = storeID;
	}

	public void clear() {
		name = null;
		email = null;
		phone = null;
		photo = null;
		signature = null;
	}
}
