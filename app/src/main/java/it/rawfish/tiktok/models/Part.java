package it.rawfish.tiktok.models;


public class Part {
	long id;
	long price;

	String name;

	public Part(long id, long price, String name) {
		this.id = id;
		this.price = price;
		this.name = name;
	}

	public Part(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getPrice() {
		return price;
	}

	public void setPrice(long price) {
		this.price = price;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
}
