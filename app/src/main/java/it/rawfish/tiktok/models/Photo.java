package it.rawfish.tiktok.models;


public class Photo {
	String picture;

	public Photo(String picture) {
		this.picture = picture;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}
}
