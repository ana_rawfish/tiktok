package it.rawfish.tiktok.models;


public class PostInvoice {
	Invoice invoice;

	public PostInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}
}
