package it.rawfish.tiktok.models;


import it.rawfish.tiktok.utils.AppUtils;

public class Price {

	long battery;
	long total;
	long bracelet;
	long paid;
	long due;
	long service;
	long part;

	public Price() {
	}

	public Price(long battery, long total, long bracelet, long paid, long due, long service, long part) {
		this.battery = battery;
		this.total = total;
		this.bracelet = bracelet;
		this.paid = paid;
		this.due = due;
		this.service = service;
		this.part = part;
	}

	public long getBattery_price() {
		return battery;
	}

	public void setBattery_price(long battery_price) {
		this.battery = battery_price;
	}

	public String getFormattedBatteryPrice() {
		return AppUtils.convertLongPriceToString(battery);
	}

	public long getTotal_price() {
		return total;
	}

	public String getFormattedTotalPrice() {
		return AppUtils.convertLongPriceToString(total);
	}

	public void setTotal_price(long total_price) {
		this.total = total_price;
	}

	public long getBracelet_price() {
		return bracelet;
	}

	public String getFormattedBraceletPrice() {
		return AppUtils.convertLongPriceToString(bracelet);
	}

	public void setBracelet_price(long bracelet_price) {
		this.bracelet = bracelet_price;
	}

	public long getDeposit() {
		return paid;
	}

	public String getFormattedDeposit() {
		return AppUtils.convertLongPriceToString(paid);
	}

	public void setDeposit(long deposit) {
		this.paid = deposit;
	}

	public long getPay_on_delivery() {
		return due;
	}

	public String getFormattedPayOnDeliveryPrice() {
		return AppUtils.convertLongPriceToString(due);
	}

	public void setPay_on_delivery(long pay_on_delivery) {
		this.due = pay_on_delivery;
	}

	public long getService() {
		return service;
	}

	public String getFormattedServicePrice() {
		return AppUtils.convertLongPriceToString(service);
	}

	public void setService(long service) {
		this.service = service;
	}

	public long getPart() {
		return part;
	}

	public void setPart(long part) {
		this.part = part;
	}
}
