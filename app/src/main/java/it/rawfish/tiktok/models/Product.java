package it.rawfish.tiktok.models;

import java.util.ArrayList;

public class Product {
	String brand;
	String bracelet;
	String model;
	String serial_number;
	String plat;
	String product_type;
	String gender;
	String notes;

	ArrayList<Photo> photos;

	public Product() {
		this.brand = "";
		this.bracelet = "";
		this.model = "";
		this.serial_number = "";
		this.plat = "";
		this.product_type = "";
		this.gender = "";
		this.notes = "";
		this.photos = new ArrayList<>();
	}

	public Product(String brand, String bracelet, String model, String serial_number, String plat, String type, String gender, String notes) {
		this.brand = brand;
		this.bracelet = bracelet;
		this.model = model;
		this.serial_number = serial_number;
		this.plat = plat;
		this.product_type = type;
		this.gender = gender;
		this.notes = notes;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getBracelet() {
		return bracelet;
	}

	public void setBracelet(String bracelet) {
		this.bracelet = bracelet;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getSerial_number() {
		return serial_number;
	}

	public void setSerial_number(String serial_number) {
		this.serial_number = serial_number;
	}

	public String getPlat() {
		return plat;
	}

	public void setPlat(String plat) {
		this.plat = plat;
	}

	public String getType() {
		return product_type;
	}

	public void setType(String type) {
		this.product_type = type;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getProduct_type() {
		return product_type;
	}

	public void setProduct_type(String product_type) {
		this.product_type = product_type;
	}

	public ArrayList<Photo> getPhotos() {
		return photos;
	}

	public void setPhotos(ArrayList<Photo> photos) {
		this.photos = photos;
	}
}
