package it.rawfish.tiktok.models;


import java.util.ArrayList;

public class Service {
	String briefing;
	String activity;
	String details;
	String delivery;
	String guarantee;

	ArrayList<Inventory> items;
	ArrayList<Part> parts;

	public Service() {
		this.briefing = "";
		this.activity = "";
		this.details = "";
		this.delivery = "";
		this.guarantee = "";
		this.items = new ArrayList<>();
		this.parts = new ArrayList<>();
	}

	public Service(String briefing, String activity, String details, String delivery, String guarantee, ArrayList<Inventory> items, ArrayList<Part> parts) {
		this.briefing = briefing;
		this.activity = activity;
		this.details = details;
		this.delivery = delivery;
		this.guarantee = guarantee;
		this.items = items;
		this.parts = parts;
	}

	public String getBriefing() {
		return briefing;
	}

	public void setBriefing(String briefing) {
		this.briefing = briefing;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getDelivery() {
		return delivery;
	}

	public void setDelivery(String delivery) {
		this.delivery = delivery;
	}

	public ArrayList<Part> getParts() {
		return parts;
	}

	public void setParts(ArrayList<Part> parts) {
		this.parts = parts;
	}

	public String getGuarantee() {
		return guarantee;
	}

	public void setGuarantee(String guarantee) {
		this.guarantee = guarantee;
	}

	public ArrayList<Inventory> getItems() {
		return items;
	}

	public void setItems(ArrayList<Inventory> items) {
		this.items = items;
	}

	public long getTotalPartsPrice() {
		long total = 0;
		if (parts != null) {
			for (Part part : parts) {
				total = total + part.price;
			}
		}

		return total;
	}

	public Inventory getSelectedInventory(String type) {
		if (items == null) {
			return null;
		} else {
			for (Inventory inventory : items) {
				if (inventory.getItem_type() != null) {
					if (inventory.getItem_type().equals(type))
						return inventory;
				}
			}

			return null;
		}
	}
}
