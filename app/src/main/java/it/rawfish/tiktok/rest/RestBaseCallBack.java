package it.rawfish.tiktok.rest;

import android.view.View;
import android.widget.ProgressBar;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class RestBaseCallBack<T> implements Callback<T> {
	private ProgressBar mProgress;

	public RestBaseCallBack() {
	}

	public RestBaseCallBack(ProgressBar progress) {
		mProgress = progress;
		mProgress.setVisibility(View.VISIBLE);
	}

	@Override
	public void onResponse(Call call, Response response) {
		if (response.isSuccessful()) {
			onSuccess((T)response.body());
		} else {
			try {
				onError(response.errorBody().string());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		if (mProgress != null)
			mProgress.setVisibility(View.GONE);
	}

	@Override
	public void onFailure(Call call, Throwable t) {
		onError(t.getMessage());
		if (mProgress != null)
			mProgress.setVisibility(View.GONE);
	}

	public abstract void onSuccess(T obj);
	public abstract void onError(String error);
}
