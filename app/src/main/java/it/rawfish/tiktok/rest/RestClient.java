package it.rawfish.tiktok.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.lang.reflect.Modifier;
import java.util.concurrent.TimeUnit;

import it.rawfish.tiktok.BuildConfig;
import it.rawfish.tiktok.utils.Constants;
import it.rawfish.tiktok.utils.SharedPrefsUtils;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {
	private static RestClientInterface mEndpoints;
	private static Retrofit mRetrofit;
	private static RestClient mInstance;
	private static OkHttpClient mDefaultHttpClient;

	static {
		setupRestClient();
	}

	private RestClient() {
		mInstance = this;
	}

	public static RestClientInterface get() {
		return mEndpoints;
	}

	public static RestClient getClient() {
		return mInstance;
	}


	private static void setupRestClient() {
		HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
		interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

		mDefaultHttpClient = new OkHttpClient.Builder()
				.readTimeout(5 * 60, TimeUnit.SECONDS)
				.connectTimeout(5 * 60, TimeUnit.SECONDS)
				.addInterceptor(interceptor)
				.addInterceptor(new Interceptor() {
					@Override
					public Response intercept(Chain chain) throws IOException {
						Request.Builder builder = chain.request().newBuilder();

						if (!SharedPrefsUtils.get().getInnerToken().isEmpty()) {
							builder.addHeader("Authorization", "Bearer " + SharedPrefsUtils.get().getInnerToken());
						}

						builder.addHeader("Content-Type", "application/json").build();
						return chain.proceed(builder.build());
					}
				}).build();

		Gson gson = new GsonBuilder()
				.setLenient()
				.serializeNulls()
				.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
				.create();

		mRetrofit = new Retrofit.Builder()
				.addConverterFactory(GsonConverterFactory.create(gson))
				.client(mDefaultHttpClient)
				.baseUrl(BuildConfig.BASE_URL)
				.build();


		mEndpoints = mRetrofit.create(RestClientInterface.class);
	}

	public static Retrofit getRetrofit() {
		return mRetrofit;
	}
}
