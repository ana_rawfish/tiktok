package it.rawfish.tiktok.rest;

import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.List;
import java.util.WeakHashMap;

import it.rawfish.tiktok.models.Employee;
import it.rawfish.tiktok.models.Inventory;
import it.rawfish.tiktok.models.Invoice;
import it.rawfish.tiktok.models.InvoiceInitializers;
import it.rawfish.tiktok.models.Price;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

public interface RestClientInterface {
	@POST("oauth/token?grant_type=password")
	@Headers("Content-Type: application/json; charset=UTF-8")
	Call<JsonObject> postUserCredentials(@Body JsonObject username);

	@POST("oauth/revoke")
	Call<JsonObject> postRevokeToken();

	@GET("api/v1/users")
	Call<Employee> getUserData();

	@Multipart
	@POST("api/v1/users")
	Call<Employee> postNewEmployee(@PartMap() HashMap<String, RequestBody> data);

	@GET("api/v1/invoices")
	Call<List<Invoice>> getInvoices(@QueryMap() WeakHashMap<String, String> opts);

	@GET("api/v1/in_progress")
	Call<List<Invoice>> getPendingInvoices();

	@PUT("api/v1/invoices/{id}")
	Call<JsonObject> updateInvoiceStatus(@Path("id") long id, @Body() HashMap<String, String> status);

	@PUT("api/v1/invoices/{id}")
	Call<Invoice> updateInvoicePrice(@Path("id") long id, @Body() HashMap<String, Price> price);

	@Multipart
	@POST("api/v1/invoices")
	Call<JsonObject> postInvoice(@PartMap() HashMap<String, RequestBody> data);

	@GET("api/v1/inventories")
	Call<List<Inventory>> getInventories(@QueryMap() WeakHashMap<String, String> opts);

	@GET("api/v1/initializes")
	Call<InvoiceInitializers> getInvoiceInitData();

	@POST("api/v1/invoices/{id}/send_email")
	Call<JsonObject> postInvoiceToCustomer(@Path("id") long id);
}