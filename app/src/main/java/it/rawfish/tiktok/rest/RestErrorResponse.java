package it.rawfish.tiktok.rest;

import com.google.gson.annotations.SerializedName;

public class RestErrorResponse {
	@SerializedName("status")
	private String status;

	@SerializedName("error")
	private String error;

	public String getStatus() {
		return status;
	}

	public String getMessage() {
		return error;
	}

	@Override
	public String toString() {
		return "ErrorResponse {" +
				"status='" + status + '\'' +
				", message='" + error + '\'' +
				'}';
	}
}
