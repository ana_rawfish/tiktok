package it.rawfish.tiktok.rest;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

public class RestErrorUtil {
	public static RestErrorResponse parseError(RestClient client, Response<?> response) {

		Converter<ResponseBody, RestErrorResponse> converter = RestClient.getRetrofit()
				.responseBodyConverter(RestErrorResponse.class, new Annotation[0]);

		RestErrorResponse error;

		try {
			error = converter.convert(response.errorBody());
		} catch (IOException e) {
			return new RestErrorResponse();
		}

		return error;
	}
}
