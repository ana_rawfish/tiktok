package it.rawfish.tiktok.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import it.rawfish.tiktok.models.Employee;
import it.rawfish.tiktok.models.Invoice;
import it.rawfish.tiktok.models.NewEmployee;
import it.rawfish.tiktok.models.Photo;
import it.rawfish.tiktok.models.Price;
import it.rawfish.tiktok.models.Product;
import it.rawfish.tiktok.models.Service;
import it.rawfish.tiktok.rest.RestBaseCallBack;
import it.rawfish.tiktok.rest.RestClient;
import it.rawfish.tiktok.utils.Constants;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class GenerateDataIntentService extends IntentService {
	public static final String MULTIPART_FORM_DATA = "multipart/form-data";
	public static final String IMAGE_DATA = "image/*";

	protected android.support.v4.os.ResultReceiver mReceiver;

	public GenerateDataIntentService() {
		super(GenerateDataIntentService.class.getSimpleName());
	}


	@Override
	protected void onHandleIntent(Intent intent) {
		mReceiver = intent.getParcelableExtra(Constants.RECEIVER);
		if (intent.getStringExtra(Constants.ROLE).equals(Constants.ROLE_ADMIN)) {
			generateInvoice();
		} else {
			generateEmployee();
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	private void deliverResultToReceiver(int resultCode, long id, String resultMessage) {
		if (mReceiver != null) {
			Bundle bundle = new Bundle();
			bundle.putString(Constants.GENERATED_LINK, resultMessage);
			bundle.putLong(Constants.GENERATED_ID, id);
			mReceiver.send(resultCode, bundle);
			mReceiver = null;
		}
	}

	private void generateInvoice() {
		ArrayList<String> images = new ArrayList<>();

		if (Invoice.get().getProduct() != null) {
			if (Invoice.get().getProduct().getPhotos() != null) {
				for (Photo photo : Invoice.get().getProduct().getPhotos()) {
					images.add(photo.getPicture());
				}
			}
		} else {
			Invoice.get().setProduct(new Product());
		}

		if (Invoice.get().getService() == null)
			Invoice.get().setService(new Service());

		if (Invoice.get().getPrice() == null)
			Invoice.get().setPrice(new Price());

		HashMap<String, RequestBody> dataMap = getImagesMap(images);
		dataMap.put("signature", getInvoiceSignatureRequestBody());
		dataMap.put("invoice", getInvoiceRequestBody());

		RestClient.get().postInvoice(dataMap).enqueue(new RestBaseCallBack<JsonObject>() {
			@Override
			public void onSuccess(JsonObject obj) {
				Log.d("postInvoice", "SUCCESS");
				if (obj.has("pdf")) {
					deliverResultToReceiver(
							Constants.SUCCESS_RESULT,
							obj.get("id").getAsLong(),
							obj.get("pdf").getAsString());
				} else {
					deliverResultToReceiver(Constants.FAILURE_RESULT, 0, "fail");
				}
			}

			@Override
			public void onError(String error) {
				Log.e("postInvoice", "ERROR: " + error);
				deliverResultToReceiver(Constants.FAILURE_RESULT, 0, "fail");
			}
		});
	}

	private void generateEmployee() {
		HashMap<String, RequestBody> dataMap = new HashMap<>();
		dataMap.put("name", createPartFromString(NewEmployee.getInstance().getName()));
		dataMap.put("phone", createPartFromString(NewEmployee.getInstance().getPhone()));
		dataMap.put("email", createPartFromString(NewEmployee.getInstance().getEmail()));
		dataMap.put("store_id", createPartFromString(String.valueOf(NewEmployee.getInstance().getStoreID())));
		dataMap.put("signature", getEmployeeSignatureRequestBody());

		if (NewEmployee.getInstance().getPhoto() != null)
			dataMap.put("photo", getEmployeePhotoRequestBody());

		RestClient.get().postNewEmployee(dataMap).enqueue(new RestBaseCallBack<Employee>() {
			@Override
			public void onSuccess(Employee obj) {
				Log.d("postNewEmployee", "SUCCESS");
				deliverResultToReceiver(Constants.SUCCESS_RESULT, obj.getId(), obj.getUsername());
			}

			@Override
			public void onError(String error) {
				Log.e("postNewEmployee", "ERROR: " + error);
				deliverResultToReceiver(Constants.FAILURE_RESULT, 0, "fail");
			}
		});
	}

	private HashMap<String, RequestBody> getImagesMap(ArrayList<String> selectedImages) {
		HashMap<String, RequestBody> map = new HashMap<>(selectedImages.size());

		for (int i = 0; i < selectedImages.size(); ++i) {
			File imgFile = new File(selectedImages.get(i));
			map.put("image_" + String.valueOf(i), RequestBody.create(MediaType.parse(IMAGE_DATA), imgFile));
		}

		return map;
	}

	@NonNull
	private RequestBody getInvoiceSignatureRequestBody() {
		return RequestBody.create(MediaType.parse(IMAGE_DATA),
				new File(Invoice.get().getCustomer().geSignature()));
	}

	@NonNull
	private RequestBody getInvoiceRequestBody() {
		return RequestBody.create(
						MediaType.parse(MULTIPART_FORM_DATA), new Gson().toJson(Invoice.get()));
	}

	@NonNull
	private RequestBody getEmployeeSignatureRequestBody() {
		return RequestBody.create(MediaType.parse(IMAGE_DATA),
				new File(NewEmployee.getInstance().getSignature()));
	}

	@NonNull
	private RequestBody getEmployeePhotoRequestBody() {
		return RequestBody.create(MediaType.parse(IMAGE_DATA),
				new File(NewEmployee.getInstance().getPhoto()));
	}

	@NonNull
	private RequestBody createPartFromString(String string) {
		return RequestBody.create(
				MediaType.parse(MULTIPART_FORM_DATA), string);
	}
}
