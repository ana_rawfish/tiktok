package it.rawfish.tiktok.services;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.os.ResultReceiver;

import it.rawfish.tiktok.utils.Constants;

public abstract class GenerateDataResultReceiver extends ResultReceiver {
	private String CREATOR = "it.rawfish.tiktok";

	public GenerateDataResultReceiver(Handler handler) {
		super(handler);
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		onReceiveResult(resultCode, resultData.getLong(Constants.GENERATED_ID), resultData.getString(Constants.GENERATED_LINK));
	}

	public abstract void onReceiveResult(int resultCode, long id, String generated);
}
