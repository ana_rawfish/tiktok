package it.rawfish.tiktok.utils;


import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Patterns;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import it.rawfish.tiktok.R;
import it.rawfish.tiktok.models.Inventory;

public class AppUtils {
	private static final int WARRANTY_STEP = 3;
	private static final int WARRANTY_MAX_MONTHS = 36;

	public static boolean isEmailValid(String email) {
		return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
	}

	public static boolean isPhoneValid(String phone) {
		return Patterns.PHONE.matcher(phone).matches();
	}

	public static long convertStringPriceToLong(String stringPrice) {
		String cleanString = stringPrice.replaceAll("[,.Rp\\s]", "");

		if (cleanString.isEmpty()) {
			return 0;
		} else {
			return Long.parseLong(cleanString);
		}
	}

	public static String convertLongPriceToString(long longPrice) {
		String formatted = NumberFormat.getNumberInstance().format(longPrice);
		return formatted + " Rp";
	}

	public static List<Inventory> getFilteredInventories(List<Inventory> inventories, String type) {
		List<Inventory> filtered = new ArrayList<>();

		for (Inventory inventory : inventories) {
			if (inventory.getItem_type().equals(type))
				filtered.add(inventory);
		}

		return filtered;
	}

	public static ArrayList<String> getWarrantyMonths(String unit) {
		ArrayList<String> months = new ArrayList<>();

		int i = WARRANTY_STEP;
		while (i < WARRANTY_MAX_MONTHS + 1) {
			if (i % WARRANTY_STEP == 0)
				months.add(String.valueOf(i + " " + unit));

			++i;
		}

		return months;
	}

	public static void openPdf(Context context, @NotNull String url) {
		String googleDocsPrefix = "http://docs.google.com/viewer?url=";
		String pdfUrl = url.replace("\"", "");

		StringBuilder stringBuilder = new StringBuilder();

		stringBuilder.append(googleDocsPrefix);
		stringBuilder.append(pdfUrl);

		String finalString = stringBuilder.toString();

		Intent intentView = new Intent(Intent.ACTION_VIEW, Uri.parse(finalString));

		Intent intent = Intent.createChooser(intentView, "Open File");

		try {
			context.startActivity(intent);
		} catch (ActivityNotFoundException e) {
			Toast.makeText(context, R.string.pdf_viewer_not_found, Toast.LENGTH_LONG).show();
		}
	}
}
