package it.rawfish.tiktok.utils;


import android.databinding.BindingAdapter;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import it.rawfish.tiktok.adapters.GridImagesAdapter;
import it.rawfish.tiktok.models.Photo;

public class Binder {

	@BindingAdapter(value = "remoteImage")
	public static void bindRemoteImage(ImageView view, String url) {
		if (url != null && !url.isEmpty()) {
			Picasso.with(view.getContext()).load(url).into(view);
		}
	}

	public static void bindPhotosGrid(RecyclerView view, ArrayList<Photo> photos, boolean isRemote, boolean isLastPh) {
		if (photos != null && photos.size() > 0) {
			ArrayList<String> paths = new ArrayList<>();
			for (Photo photo : photos) {
				paths.add(photo.getPicture());
			}
			view.setAdapter(new GridImagesAdapter(
					paths,
					isRemote,
					isLastPh,
					new GridImagesAdapter.OnPhotosAdapterListener() {
						@Override
						public void onChooseMorePhotos() {
						}
					}
			));
		}
	}

	@BindingAdapter(value = "price")
	public static void bindPrice(TextView view, long price) {
		view.setText(AppUtils.convertLongPriceToString(price));
	}
}
