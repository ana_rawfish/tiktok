package it.rawfish.tiktok.utils;


public class Constants {
	public static final String PREFERENCES_NAME = "tiktok_prefs";
	public static final String PREF_INNER_TOKEN = "inner_token";
	public static final String PREF_REFRESH_TOKEN = "refresh_token";

	public static final String SPARE_TYPE_BATTERY = "battery";
	public static final String SPARE_TYPE_BRACELET = "bracelet";

	public static final String INVOICE_STATUS_PENDING = "in_progress";
	public static final String INVOICE_STATUS_DONE = "done";

	public static final String ROLE = "role";
	public static final String ROLE_EMPLOYEE = "employee";
	public static final String ROLE_ADMIN = "super_admin";

	public static final String RECEIVER = "receiver";
	public static final String GENERATED_LINK = "g_link";
	public static final String GENERATED_ID = "g_id";

	public static final int PERMISSION_CAMERA = 33;
	public static final int INTENT_REQUEST_GET_IMAGES = 44;

	public static int FAILURE_RESULT = -1;
	public static int SUCCESS_RESULT = 0;
}
