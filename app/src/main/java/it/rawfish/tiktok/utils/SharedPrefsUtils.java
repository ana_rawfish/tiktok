package it.rawfish.tiktok.utils;

import android.content.Context;
import android.content.SharedPreferences;

import it.rawfish.tiktok.TiktokApp;

public class SharedPrefsUtils {
    private static final String TAG = SharedPrefsUtils.class.getName();
    private static SharedPrefsUtils instance = null;
    private SharedPreferences mPrefs;

    protected SharedPrefsUtils() {
        mPrefs = TiktokApp.get().getSharedPreferences(Constants.PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    public static synchronized SharedPrefsUtils get() {
        if (instance == null) {
            instance = new SharedPrefsUtils();
        }

        return instance;
    }

    public String getInnerToken() {
        return this.mPrefs.getString(Constants.PREF_INNER_TOKEN, "");
    }

    public boolean setInnerToken(String value) {
        return this.mPrefs.edit().putString(Constants.PREF_INNER_TOKEN, value).commit();
    }

    public String getRefreshToken() {
        return this.mPrefs.getString(Constants.PREF_REFRESH_TOKEN, "");
    }

    public boolean setRefreshToken(String value) {
        return this.mPrefs.edit().putString(Constants.PREF_REFRESH_TOKEN, value).commit();
    }

    public void clear() {
        setInnerToken("");
        setRefreshToken("");
    }
}
