package info.androidramp.gearload;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class Loading extends LinearLayout {

	View loadingView;

	ImageView vGear0;
	ImageView vGear1;
	ImageView vGear2;

	Animation anim0;
	Animation anim1;
	Animation anim2;

	Context context;


	public Loading(Context context) {
		super(context);
		init(context, null);
	}

	public Loading(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs);
	}

	public Loading(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);

		init(context, attrs);
	}

	public void stop() {
		vGear0.setAnimation(null);
		vGear1.setAnimation(null);
		vGear2.setAnimation(null);
		loadingView.setVisibility(View.GONE);
		Animation fadeOutAnimation = AnimationUtils.loadAnimation(context, R.anim.loading_fade_out);
		loadingView.startAnimation(fadeOutAnimation);
	}

	public void start() {
		loadingView.setVisibility(View.VISIBLE);

		Animation fadeInAnimation = AnimationUtils.loadAnimation(context, R.anim.loading_fade_in);
		fadeInAnimation.setAnimationListener(new Animation.AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
			}

			@Override
			public void onAnimationEnd(Animation animation) {
				vGear0.startAnimation(anim0);
				vGear1.startAnimation(anim1);
				vGear2.startAnimation(anim2);
			}

			@Override
			public void onAnimationRepeat(Animation animation) {

			}
		});

		loadingView.startAnimation(fadeInAnimation);
	}

	private void init(Context context, AttributeSet attrs) {
		this.context = context;
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if (inflater != null) {
			loadingView = inflater.inflate(R.layout.loading, this);
		}

		vGear0 = (ImageView)loadingView.findViewById(R.id.gear_green);
		vGear1 = (ImageView)loadingView.findViewById(R.id.gear_blue);
		vGear2 = (ImageView)loadingView.findViewById(R.id.gear_red);

		if (attrs != null) {
			TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.loading_gear);

			int gear0Color = typedArray.getColor(R.styleable.loading_gear_gear_0_color, 0);
			int gear1Color = typedArray.getColor(R.styleable.loading_gear_gear_1_color, 0);
			int gear2Color = typedArray.getColor(R.styleable.loading_gear_gear_2_color, 0);

			int gear0Size = typedArray.getDimensionPixelSize(R.styleable.loading_gear_gear_0_size, LayoutParams.WRAP_CONTENT);
			int gear0marginTop = typedArray.getDimensionPixelSize(R.styleable.loading_gear_gear_0_marginTop, 0);
			int gear0marginBottom = typedArray.getDimensionPixelSize(R.styleable.loading_gear_gear_0_marginBottom, 0);
			int gear0marginStart = typedArray.getDimensionPixelSize(R.styleable.loading_gear_gear_0_marginStart, 0);
			int gear0marginEnd = typedArray.getDimensionPixelSize(R.styleable.loading_gear_gear_0_marginEnd, 0);

			int gear1Size = typedArray.getDimensionPixelSize(R.styleable.loading_gear_gear_1_size, LayoutParams.WRAP_CONTENT);
			int gear1marginTop = typedArray.getDimensionPixelSize(R.styleable.loading_gear_gear_1_marginTop, 0);
			int gear1marginBottom = typedArray.getDimensionPixelSize(R.styleable.loading_gear_gear_1_marginBottom, 0);
			int gear1marginStart = typedArray.getDimensionPixelSize(R.styleable.loading_gear_gear_1_marginStart, 0);
			int gear1marginEnd = typedArray.getDimensionPixelSize(R.styleable.loading_gear_gear_1_marginEnd, 0);

			int gear2Size = typedArray.getDimensionPixelSize(R.styleable.loading_gear_gear_2_size, LayoutParams.WRAP_CONTENT);
			int gear2marginTop = typedArray.getDimensionPixelSize(R.styleable.loading_gear_gear_2_marginTop, 0);
			int gear2marginBottom = typedArray.getDimensionPixelSize(R.styleable.loading_gear_gear_2_marginBottom, 0);
			int gear2marginStart = typedArray.getDimensionPixelSize(R.styleable.loading_gear_gear_2_marginStart, 0);
			int gear2marginEnd = typedArray.getDimensionPixelSize(R.styleable.loading_gear_gear_2_marginEnd, 0);

			RelativeLayout.LayoutParams params0 = new RelativeLayout.LayoutParams(
					gear0Size,
					gear0Size
			);
			params0.setMargins(gear0marginStart, gear0marginTop, gear0marginEnd, gear0marginBottom);
			vGear0.setLayoutParams(params0);

			RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(
					gear1Size,
					gear1Size
			);
			params1.setMargins(gear1marginStart, gear1marginTop, gear1marginEnd, gear1marginBottom);
			vGear1.setLayoutParams(params1);

			RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(
					gear2Size,
					gear2Size
			);
			params2.setMargins(gear2marginStart, gear2marginTop, gear2marginEnd, gear2marginBottom);
			vGear2.setLayoutParams(params2);

			if (gear0Color != 0)
				vGear0.setColorFilter(gear0Color);

			if (gear1Color != 0)
				vGear1.setColorFilter(gear1Color);

			if (gear2Color != 0)
				vGear2.setColorFilter(gear2Color);

			typedArray.recycle();
		}

		anim0 = AnimationUtils.loadAnimation(context, R.anim.loading_rotate_clock);
		anim1 = AnimationUtils.loadAnimation(context, R.anim.loading_rotate_anti_clock);
		anim2 = AnimationUtils.loadAnimation(context, R.anim.loading_rotate_clock);

		if (!isInEditMode()) {
			loadingView.setVisibility(GONE);
		}
	}
}
